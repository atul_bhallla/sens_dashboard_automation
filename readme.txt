Run specific test case with report file path: pytest TestCases/test_alert_page.py -v --html=C:\Users\atul.bhalla\PycharmProjects\sens-dashboard-automation\Reports\sensweb.html

Run all the test cases in TestCases folder with report file path: pytest C:\Users\atul.bhalla\PycharmProjects\sens-dashboard-automation\TestCases -v --html=C:\Users\atul.bhalla\PycharmProjects\sens-dashboard-automation\Reports\sensweb.html

Run specific test case without report: pytest TestCases/test_alert_page.py

Check the code coverage for the project: coverage run -m pytest ....
coverage html




