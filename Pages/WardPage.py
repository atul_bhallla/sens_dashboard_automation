from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import invisibility_of_element_located
from selenium.webdriver.support.wait import WebDriverWait
from Pages.BasePage import BasePage

class WardPage(BasePage):

    HEARDER = (By.XPATH, "//a[@class='logo']")
    ACCOUNT_NAME = (By.XPATH, "//div[@class='user-picture initials ng-star-inserted']")
    WARDCOUNT = (By.CLASS_NAME, "link-cursor")
    SHOW_EMPTY_WARD = (By.XPATH, "//div[@class='visibilityIconFlex']")
    PATIENT_COUNT_IN_WARD = (By.XPATH, "//p[@class='text-white font-weight-600 margin-left-small']")
    PATIENT_COUNT_PER_PAGE = (By.XPATH,"/html/body/ngx-app/ngx-pages/ngx-one-column-layout/nb-layout/div[1]/div/div/div/div/nb-layout-column/ngx-vitals/nb-card/nb-card-body/mat-paginator/div/div/div[2]/div")
    ORG_NAME = (By.XPATH, "//div[@class='org-data']//span[2]")
    VITAL_PAGE_HEAD_PATIENT_COUNT = (By.CLASS_NAME, "dz-card")
    WARDNAME = (By.XPATH, "//div[@class='h5 text-white mb-2 text-white']")
    WARD_NAME_AND_PATIENT_NUMBER = (By.XPATH, "//div[@class='user-picture initials ng-star-inserted']//following::span[1]")
    REPORTS_TAB = (By.XPATH, "//span[text()='Alerts']")
    WARDNAME_TEXT = (By.XPATH, "//nb-card-header[@class='dz-card']//h4")

    def __init__(self, driver):
        super().__init__(driver)

    def get_header(self):
        account_name = ""
        if self.is_visible(self.HEARDER):
            header_sens = self.get_element_text(self.HEARDER)
            print("Secure RPM header is available")
        else:
            print("Secure RPM header is not available")

        if self.is_visible(self.ACCOUNT_NAME):
            account_name = self.get_element_text(self.ACCOUNT_NAME)
        return account_name

    def get_org_name(self):
        if self.is_visible(self.ORG_NAME):
            org_name = self.get_element_text(self.ORG_NAME)
            print("Org name is : " + org_name)
        else:
            print("Org name is not available")

    def ward_patient_vitals(self):
        self.is_visible(self.WARDCOUNT)
        d1 = self.get_elements(self.WARDCOUNT)
        no_of_wards = len(d1)
        print("Number of wards on ward page : " + str(no_of_wards))
        patient_count_in_ward = self.get_elements(self.PATIENT_COUNT_IN_WARD)
        no_of_wards_patients = len(patient_count_in_ward)
        if self.is_visible(self.SHOW_EMPTY_WARD)==True:
            self.do_click(self.SHOW_EMPTY_WARD)
            print("Show Empty Wards button is available")
        else:
            print("Show Empty Wards button is not available")
        patientsInWards = self.get_elements(self.PATIENT_COUNT_IN_WARD)
        patient_count_list = []
        for item in patientsInWards:
            patient_count_list.append(item.text)
        assert '0 Patients' in patient_count_list, f"'0 Patients' are not available in Empty ward"
        wardname =self.get_elements(self.WARDNAME)
        ward_name_list = []
        for name in wardname:
            ward_name_list.append(name.text)
        assert 'All' and 'Uncategorized Wards' in ward_name_list, f"'All' and 'Uncategorized Wards' wards are not available"
        # get_wards()

        for i in range(0, no_of_wards):
            self.is_visible(self.WARDCOUNT)
            wards_count = self.get_elements(self.WARDCOUNT)
            patient = []
            for a in range(0, no_of_wards_patients):
                self.is_visible(self.PATIENT_COUNT_IN_WARD)
                c1 = self.get_elements(self.PATIENT_COUNT_IN_WARD)
                c2 = (c1[a].text)
                patient.append(c2.split(" ")[0])
            print("Clicked this ward : " + wards_count[i].text)
            wards_count[i].click()
            ward_and_patient = self.get_element_text(self.WARD_NAME_AND_PATIENT_NUMBER)
            print(ward_and_patient)
            # wait.until(presence_of_element_located((By.TAG_NAME,"span")))
            if self.driver.find_elements(By.XPATH,
                                         "//div[@class='mat-chip-list-wrapper']//mat-icon"):
                for i in self.driver.find_elements(By.XPATH,
                                                   "//div[@class='mat-chip-list-wrapper']//mat-icon"):
                    self.driver.find_element(By.XPATH,
                                             "//div[@class='mat-chip-list-wrapper']//mat-icon").click()
            else:
                print("Filter is not selected")
            vital_page_patient_count = self.get_element(self.VITAL_PAGE_HEAD_PATIENT_COUNT)
            WebDriverWait(self.driver, 10).until(invisibility_of_element_located((By.CLASS_NAME, "nb-spinner-container")))
            span = vital_page_patient_count.find_element(By.TAG_NAME, "span").text
            no_of_vitals_patients = span.split(" ")[0]
            ward_name_text = self.get_element_text(self.WARDNAME_TEXT)
            print("Number of patients in the " + ward_name_text + " ward is : " + no_of_vitals_patients)
            assert no_of_vitals_patients in patient, f"Number of patients in the wards and number of patients in the ward head are not same"
            self.is_visible(self.PATIENT_COUNT_PER_PAGE)
            no_of_patients_in_page = self.get_element(self.PATIENT_COUNT_PER_PAGE)
            no_of_records = no_of_patients_in_page.text.split(" ")[-1]
            assert no_of_vitals_patients == no_of_records, f"No of patients is incorrect for {wards_count[i].text}"
            self.do_click(self.REPORTS_TAB)
            assert no_of_vitals_patients in ward_and_patient, f"Left side Ward name & Patients are same as head Ward name & Patients"
            self.driver.back()
            self.driver.back()