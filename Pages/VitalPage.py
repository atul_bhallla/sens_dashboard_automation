import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.color import Color
from Pages.BasePage import BasePage
from selenium.webdriver.common.keys import Keys
import random
from datetime import datetime

class VitalPage(BasePage):

    VITAL_TAB = (By.XPATH, "//a[@title='Vitals']")
    REFRESH_BUTTON = (By.XPATH, "//button[@class='right-margin appearance-filled size-small status-info shape-round icon-start transitions']")
    DOWNLOAD_BUTTON = (By.XPATH, "//button[@class='right-margin font-sm appearance-filled size-small status-primary shape-round transitions']")
    ADD_PATIENTS_BUTTON = (By.XPATH, "//button[@class='right-margin margin-bottom-sm font-sm appearance-filled size-small status-info shape-round transitions']")
    DEVICE_NAME_COLUMN = (By.XPATH, "//button[@class='right-margin font-sm appearance-filled size-small status-primary shape-round transitions']//following::div[2]")
    PATIENT_NAME_ID_COLUMN = (By.XPATH, "//div[text()='Patient Name & ID']")
    BED_NUMBER_COLUMN = (By.XPATH, "//button[text()='Bed No.']")
    DEWS_COLUMN = (By.XPATH, "//button[text()='DEWS']")
    HEART_COLUMN = (By.XPATH, "//button[text()='Heart']")
    RESPIRATION_COLUMN = (By.XPATH, "//button[text()='Respiration']")
    SPO2_COLUMN = (By.XPATH, "//button[text()='SpO']")
    NIBP_COLUMN = (By.XPATH, "//button[text()='SpO']//following::button[1]")
    TEMP_COLUMN = (By.XPATH, "//button[text()='Temp.']")
    ACTION_COLUMN = (By.XPATH, "//th[text()='Action']")
    INPUT_FIELD_VITAL = (By.XPATH, "//nb-select[@id= 'filterByName']//following::input[1]")
    NEXT_BUTTON_ADD_PATIENT = (By.XPATH, "//button[@class='appearance-outline size-small status-primary shape-round transitions']")
    CLOSE_BUTTON_ADD_PATIENT = (By.XPATH, "//button[@class='appearance-outline size-small status-primary shape-round transitions']")
    ACTION_BUTTON = (By.XPATH, "//button[@aria-label='Action items']")
    REPORTS_BUTTON = (By.XPATH, "//span[text()='Reports ']/../.")
    REMOVE_PATIENT_BUTTON = (By.XPATH, "//span[text()='Remove Patient']/../.")
    DISCHARGED_RADIO_BUTTON = (By.XPATH, "//input[@value='Discharged']//following::span[2]")
    YES_BUTTON = (By.XPATH, "//button[text()=' Yes ']")
    FIRST_NAME = (By.ID, "mat-input-3")
    LAST_NAME = (By.XPATH, "//input[@name='LastName']")
    PATIENT_ID = (By.ID, "mat-input-5")
    EMAIL = (By.XPATH, "//input[@name='Email']")
    PHONE = (By.XPATH, "//input[@id='phone']")
    DEVICE_NAME_PATIENT_PAGE = (By.XPATH, "//p[@class='ng-star-inserted']")
    PATIENT_NAME_PATIENT_PAGE = (By.XPATH, "//p[@class='ng-star-inserted']//following::p[2]")
    DOZEE_AVAILABLE_DEVICES = (By.ID, "mat-select-3")
    DOZEE_SEARCH_BOX_ADD_PATIENT = (By.ID, "mat-input-10")
    DOCTOR_NAME_INPUT = (By.XPATH, "//input[@name='ConsultingDoctor']")
    MEDICAL_CONDITION_INPUT = (By.XPATH, "//input[@id='mat-chip-list-input-0']")
    HIGH_HEART_RATE_TOGGLE = (By.XPATH, "//mat-slide-toggle[@id='mat-slide-toggle-6']")
    ESCALATE_INPUT_FIELD = (By.XPATH, "//input[@placeholder='Email/ Mobile/ Webhook']")
    ADD_BUTTON = (By.XPATH, "//button[@class='mt-2 appearance-filled size-medium status-primary shape-round transitions']")
    MEDICAL_CONDITION_TEXT = (By.XPATH, "//div[@class='mat-chip-list-wrapper']")
    WARD_NAME_TEXT = (By.XPATH, "//div[@class='mat-chip-list-wrapper']//following::p[2]")
    BED_NUMBER_TEXT = (By.XPATH, "//div[@class='mat-chip-list-wrapper']//following::p[3]")
    DOCTOR_NAME_TEXT = (By.XPATH, "//div[@class='mat-chip-list-wrapper']//following::p[4]")
    ALERT_BUTTON_PATIENT_PAGE = (By.XPATH, "//h6[text()='Sleeps']//following::div[4]")
    ALERT_TEXT_PATIENT_PAGE = (By.XPATH, "//div[@class='col-1 col-md-2 col-sm-3 alert-status']")
    NOTIFIER_PATIENT_PAGE = (By.XPATH, "//h6[text()='Notifiers']//following::p")
    HEART_RATE_TEXT = (By.XPATH, "//div[text()='Heart Rate']")
    RESPIRATION_RATE_TEXT = (By.XPATH, "//div[text()='Respiration Rate']")
    SPO2_RATE_TEXT = (By.XPATH, "//div[text()='SpO2']")
    BED_OCCUPANCY_TEXT = (By.XPATH, "//div[text()='Bed Occupancy']")
    DEWS_TEXT = (By.XPATH, "//div[text()='DEWS']")
    BP_SYSTOLIC_TEXT = (By.XPATH, "//div[text()='BP(Systolic)']")
    TEMPERATURE_TEXT = (By.XPATH, "//div[text()='Temperature']")
    MOVEMENT_TEXT = (By.XPATH, "//div[text()='Movement']")
    TOGGLE = (By.XPATH, "//div[@class='mat-slide-toggle-bar mat-slide-toggle-bar-no-side-margin']")
    MANAGE_ALERTS_BUTTON = (By.XPATH, "//span[text()='Manage Alerts']/../.")
    LOW_HEART_RATE_TOGGLE = (By.XPATH, "//span[text()=' Low Heart Rate ']//following::div[4]")
    ADD_INFO_BUTTON = (By.XPATH, "//span[text()='Add Info']/../.")
    ADD_NOTES_BUTTON = (By.XPATH, "//button[text()='Add Notes']")
    ADD_NOTES_INPUT = (By.XPATH, "//label[text()='Enter Note']//following::textarea[1]")
    SAVE_BUTTON_ADD_NOTES = (By.XPATH, "//button[@class='appearance-filled size-small status-info shape-round transitions']")
    ADD_DATA_BUTTON = (By.XPATH, "//button[text()='Add Data']")
    TEMP_INPUT = (By.XPATH, "//label[text()='Temp. :']//following::input[1]")
    NOTES_PATIENT_PAGE = (By.XPATH, "//span[text()='Notes']/../.")
    NOTES_TEXT = (By.XPATH, "//div[@class='visited-date']")
    DATA_TEXT = (By.XPATH, "//table//tbody//td[2]")
    SNOOZE_BUTTON_ACTION = (By.XPATH, "//span[text()='Snooze']/..")
    SNOOZE_BUTTON = (By.XPATH, "//button[@class='ml-1 appearance-filled size-small status-primary shape-rectangle transitions']")
    SNOOZE_TEXT = (By.XPATH, "//span[text()=' Snoozed ']")
    RANGE_OF_PAGES_OUT_OF_PATIENTS = (By.XPATH, "/html/body/ngx-app/ngx-pages/ngx-one-column-layout/nb-layout/div/div/div/div/div/nb-layout-column/ngx-vitals/nb-card/nb-card-body/mat-paginator/div/div/div[2]/div")
    ADD_NOTES_BUTTON_PATIENT_PAGE = (By.XPATH, "//button[@class='right-margin appearance-ghost size-medium status-info shape-rectangle icon-start transitions']")
    ADD_DATA_BUTTON_PATIENT_PAGE = (By.XPATH, "//button[@class='appearance-ghost size-medium status-info shape-rectangle icon-start transitions']")
    MIN_DOWNGRADE_BUTTON = (By.XPATH, "//ngx-timepicker-time-control[@class='ngx-timepicker__control--third']//span[2]")
    ADD_ESCALATION = (By.XPATH, "//p[text()='Add Escalation']")
    PHONE_INPUT = (By.XPATH, "//input[@id='phone']")
    ADD_BUTTON_ESCALATION = (By.XPATH, "//button[text()='Add']")
    ALERT_ESCALATION_DROPDOWN = (By.XPATH, "//mat-label[text()='Escalate to ']//following::mat-select[1]")
    PHONE_OPTION = (By.XPATH, "//span[text()=' Mobile ']/..")
    NOTIFIER_TEXT = (By.XPATH, "//h6[text()='Notifiers']//following::p")
    OVERVIEW_BOX_HEAD_TEXT = (By.XPATH, "//div[@class='col font-title ml-3']")
    BPM_TEXT = (By.XPATH, "//span[text()='BPM']")
    RPM_TEXT = (By.XPATH, "//span[text()='RPM']")
    SPO2_TEXT = (By.XPATH, "//img[@class='icon-size']")
    BP_TEXT = (By.XPATH, "//img[@class='bp-icon-size']")
    TEMP_TEXT = (By.XPATH, "//span[text()='°F']")
    ALERTS_TEXT = (By.XPATH, "//span[@class='cursor-pointer']")
    DOWNLOAD_DATA_BUTTON = (By.XPATH, "//li[@class='active ng-star-inserted']//following::button[1]")
    DOWNLOAD_AS_PDF = (By.XPATH, "//button[text()='Download as PDF']")
    PATIENT_REPORT_OPTIONS = (By.XPATH, "//span[@class='text']")
    LAST_24_HOURS_RADIO_BUTTON = (By.XPATH, "//span[text()=' Last 24 hours ']/../..")
    REQUEST_BUTTON = (By.XPATH, "//div[text()='Request']/..")
    CANCEL_BUTTON = (By.XPATH, "//button[text()='Cancel']")
    ERROR_TEXT = (By.XPATH, "//p[@class='text-danger font-13 pl-3 text-center']")
    DOWNLOAD_AS_CSV = (By.XPATH, "//button[text()='Download as CSV']")
    DOWNLOAD_BUTTON_CSV_REPORT = (By.XPATH, "//div[text()='Download']/..")
    TRENDS_BUTTON = (By.XPATH, "//span[text()='Trends']/..")
    SHOW_BUTTON = (By.XPATH, "//button[text()='Show']")
    DOWNLOAD_DATA_TRENDS_BUTTON = (By.XPATH, "//button[text()='Show']//following::button[1]")
    SLEEP_PATIENT_BUTTON = (By.XPATH, "//h6[text()='Sleeps']/..")
    WAKUP_TIME = (By.XPATH, "//button[text()=' Wakup Time ']")
    SLEEP_SCORE = (By.XPATH, "//button[text()=' Sleep Score ']")
    HEART_RATE_SLEEP = (By.XPATH, "//button[text()=' Heart Rate ']")
    RESPIRATION_RATE_SLEEP = (By.XPATH, "//button[text()=' Respiration Rate ']")
    RECOVERY_SLEEP = (By.XPATH, "//button[text()=' Recovery ']")
    STRESS_SLEEP = (By.XPATH, "//button[text()=' Stress ']")
    REPORTS_PATIENT_PAGE = (By.XPATH, "//button[@class='appearance-outline size-medium status-info shape-semi-round transitions']")
    BP_CALIBRATE = (By.XPATH, "//a[text()='Calibrate']")
    AVERAGE_SYSTOLIC_BP = (By.XPATH, "//input[@formcontrolname='BpSys']")
    AVERAGE_DIASTOLIC_BP = (By.XPATH, "//input[@formcontrolname='BpDia']")
    AVERAGE_HEART_RATE = (By.XPATH, "//input[@formcontrolname='hrRate']")
    CALIBRATE_BUTTON = (By.XPATH, "//button[text()='Calibrate']")
    REMOVE_PATIENT_OPTION = (By.XPATH, "//nb-radio[@class='status-basic ng-star-inserted']")
    ALL_PATIENTS_TAB = (By.XPATH, "//a[@title='All Patients']")
    PATIENT_NAME_ALL_PATIENTS = (By.XPATH, "//td[@class='text-left mat-cell cdk-column-PatientId mat-column-PatientId ng-star-inserted']")
    MODIFIY_SNOOZE = (By.XPATH, "//span[text()='Modify Snooze']/../.")
    MODIFIY_SNOOZE_DURATION = (By.XPATH, "//span[text()='Duration']/../../../..//mat-select")
    ZERO_MINUTES_OPTION = (By.XPATH, "//span[text()='0 Minutes']")
    UPDATE_SNOOZE = (By.XPATH, "//button[@class='ml-1 appearance-filled size-small status-primary shape-rectangle transitions']")
    SEARCH_BUTTON = (By.XPATH, "//button[text()='Search ']")
    DROPDOWN_ALL_PATIENTS = (By.XPATH, "//p[text()='Advanced Search']//following::nb-select")
    SEARCH_INPUT_ALL_PATIENTS = (By.XPATH, "//input[@placeholder='Search by name, email or phone number']")
    DATE_OF_ADMISSION_TEXT = (By.XPATH, "//td[@class='text-left mat-cell cdk-column-DateofAdmission mat-column-DateofAdmission ng-star-inserted']")
    DATE_OF_DISCHARGE_TEXT = (By.XPATH, "//td[@class='text-left mat-cell cdk-column-DateofDischarge mat-column-DateofDischarge ng-star-inserted']")
    PATIENT_HYPERLINK_ALL_PATIENT = (By.XPATH, "//a[@class='appearance-ghost size-medium status-info shape-rectangle font-16']")
    ADMITTED_DISCHARGED_DROPDOWN = (By.XPATH, "//p[text()='Advanced Search']//following::nb-select")
    ADMITTED_DROPDOWN_OPTION = (By.XPATH, "//nb-option[@value='ADMITTED']")
    DOWNLOAD_BUTTON_ALL_PATIENT = (By.XPATH, "//button[@class='right-margin font-sm appearance-filled size-small status-primary shape-round transitions']")
    PATIENT_ID_TEXT = (By.XPATH, "//p[@class='user-info-line-item ng-star-inserted']")
    PATIENT_EXPIRED_RADIO_BUTTON = (By.XPATH, "//input[@value='Patient expired']//following::span[2]")
    SHIFTED_To_ICU_RADIO_BUTTON = (By.XPATH, "//input[@value='Shifted to ICU']//following::span[2]")
    SHIFTED_To_OTHER_WARD_OR_BED_RADIO_BUTTON = (By.XPATH, "//input[@value='Shifted to Other Ward/Bed']//following::span[2]")
    SHIFTED_To_ANOTHER_DOZEE = (By.XPATH, "//input[@value='Shifted to Another Dozee']//following::span[2]")
    DEVICE_CONTROL_DROPDOWN = (By.XPATH, "//mat-select[@placeholder='Device Control']")
    SEARCH_DEVICE_INPUT = (By.XPATH, "//input[@placeholder='Search Device ...']")
    CONFIRM_BUTTON = (By.XPATH, "//button[@class='appearance-outline size-small status-primary shape-round transitions']")


    def __init__(self, driver):
        super().__init__(driver)

    def add_patients_mandatory_fields(self, dozee_id):
        self.do_click(self.VITAL_TAB)
        print("Vitals tab is clicked")
        assert self.is_visible(self.REFRESH_BUTTON)==True, f"Refresh button is not available"
        self.do_click(self.REFRESH_BUTTON)
        assert self.is_visible(self.DOWNLOAD_BUTTON)==True, f"Download button is not available"
        assert self.is_visible(self.ADD_PATIENTS_BUTTON)==True, f"Add Patients button is not available"
        assert self.is_visible(self.DEVICE_NAME_COLUMN)==True, f"Device name column is not available"
        assert self.is_visible(self.PATIENT_NAME_ID_COLUMN) == True, f"Patient name & ID column is not available"
        assert self.is_visible(self.BED_NUMBER_COLUMN) == True, f"Bed number column is not available"
        assert self.is_visible(self.DEWS_COLUMN) == True, f"Dews column is not available"
        assert self.is_visible(self.HEART_COLUMN) == True, f"Heart column is not available"
        assert self.is_visible(self.RESPIRATION_COLUMN) == True, f"Respiration column is not available"
        assert self.is_visible(self.SPO2_COLUMN) == True, f"SpO2 column is not available"
        assert self.is_visible(self.NIBP_COLUMN) == True, f"NiBP column is not available"
        assert self.is_visible(self.TEMP_COLUMN) == True, f"Temperature column is not available"
        assert self.is_visible(self.ACTION_COLUMN) == True, f"Action column is not available"

        time.sleep(4)
        self.do_click(self.ADD_PATIENTS_BUTTON)
        print("ADD Patient button is clicked")
        patient_name = 'automation testing'
        patient_id = 'testing12345'
        self.do_send_keys(self.FIRST_NAME, patient_name)
        print("First name is set")
        self.do_send_keys(self.PATIENT_ID, patient_id)
        print("Patient ID is set")
        s = self.driver.find_elements(By.TAG_NAME, "button")
        for i in s:
            if i.text == "NEXT":
                i.click()
        print("Next button is clicked")
        # dozee_id = 'DP-3941'
        self.do_click(self.DOZEE_AVAILABLE_DEVICES)
        self.do_send_keys(self.DOZEE_SEARCH_BOX_ADD_PATIENT, dozee_id)
        if self.driver.find_element(By.XPATH, "//span[text() =' " + dozee_id + " ']"):
            self.driver.find_element(By.XPATH, "//span[text() =' " + dozee_id + " ']").click()
            print("Dozee device is set")
        else:
            print("Dozee device is not available")
        self.do_click(self.NEXT_BUTTON_ADD_PATIENT)
        time.sleep(2)
        self.do_click(self.CLOSE_BUTTON_ADD_PATIENT)
        print("Close button is clicked")
        if self.is_visible(self.CANCEL_BUTTON):
            self.do_click(self.CANCEL_BUTTON)
            print("Cancel button is clicked")
        time.sleep(4)
        # self.driver.refresh()
        self.do_click(self.REFRESH_BUTTON)
        print("Refresh button is clicked")
        time.sleep(3)
        self.do_send_keys(self.INPUT_FIELD_VITAL, dozee_id)
        print("Patient name is set")
        self.driver.find_element(By.XPATH, "//nb-select[@id= 'filterByName']//following::input[1]").send_keys(Keys.ENTER)
        time.sleep(2)
        assert self.driver.find_elements(By.XPATH, "//p[text() ='" + patient_id + "']"), f"New patient is not available"
        self.driver.find_element(By.XPATH, "//a[@class='appearance-ghost size-medium status-info shape-rectangle font-16 font-bold ng-star-inserted']").click()
        print("Patient name is clicked")
        device_name = self.get_element_text(self.DEVICE_NAME_PATIENT_PAGE)
        patient_id_text = self.get_element_text(self.PATIENT_NAME_PATIENT_PAGE)
        assert dozee_id in device_name, f"Dozee ID is not same in patient page"
        assert patient_id in patient_id_text, f"Patient ID is not same in patient page"
        print("New patient is added successfully")
        time.sleep(4)
        self.driver.back()
        time.sleep(3)


    def remove_patient_discharged(self, dozee_id):
        patient_id = 'testing12345'
        self.do_send_keys(self.INPUT_FIELD_VITAL, dozee_id)
        print("Patient name is set")
        self.driver.find_element(By.XPATH, "//nb-select[@id= 'filterByName']//following::input[1]").send_keys(
            Keys.ENTER)
        time.sleep(2)
        assert self.driver.find_elements(By.XPATH, "//p[text() ='" + patient_id + "']"), f"New patient is not available"
        self.do_click(self.ACTION_BUTTON)
        print("Action button is clicked")
        self.do_click(self.REMOVE_PATIENT_BUTTON)
        print("Remove patient button is clicked")
        remove_patient_options = self.get_elements(self.REMOVE_PATIENT_OPTION)
        remove_patient_list = []
        for options in remove_patient_options:
            remove_patient_list.append(options.text)
        print(remove_patient_list)
        remove_patient_original_option = ['Discharged', 'Shifted to ICU', 'Shifted to Other Ward/Bed', 'Patient expired', 'Shifted to Another Dozee']
        assert remove_patient_original_option == remove_patient_list, f"Remove patient options are missing"
        self.do_click(self.DISCHARGED_RADIO_BUTTON)
        print("Discharge radio button is clicked")
        time.sleep(4)
        self.do_click(self.YES_BUTTON)
        print("Yes button is clicked")
        time.sleep(4)
        self.do_click(self.ALL_PATIENTS_TAB)
        print("All Patients tab is clicked")
        self.is_visible(self.DOWNLOAD_BUTTON_ALL_PATIENT)
        download_button = self.get_element(self.DOWNLOAD_BUTTON_ALL_PATIENT)
        self.driver.execute_script("arguments[0].click();", download_button)
        print("Download button is clicked")
        removed_patient_all_patients = self.get_elements(self.PATIENT_NAME_ALL_PATIENTS)
        removed_patient_name_list = []
        for name in removed_patient_all_patients:
            removed_patient_name_list.append(name.text)
        assert patient_id in removed_patient_name_list, f"Removed patient is not available in ALL Patients"
        print("Removed patient successfully")
        self.is_visible(self.SEARCH_BUTTON)
        search_button = self.driver.find_element(By.XPATH, "//button[text()='Search ']")
        self.driver.execute_script("arguments[0].click();", search_button)
        print("Search button is clicked")
        time.sleep(4)
        from_calender_date = self.driver.find_element(By.XPATH, "//label[text()='From']//following::input[1]")
        to_calender_date = self.driver.find_element(By.XPATH, "//label[text()='To']//following::input[1]")
        time.sleep(3)
        min_date = datetime.strptime(from_calender_date.get_attribute("value"), '%d/%m/%Y')
        max_date = datetime.strptime(to_calender_date.get_attribute("value"), '%d/%m/%Y')
        number_of_days = max_date - min_date
        assert number_of_days.days == 15, "Date difference in calender is not 15 days"
        drop_down = self.driver.find_element(By.XPATH, "//p[text()='Advanced Search']//following::nb-select").get_attribute("textContent")
        assert drop_down == 'Discharged' , f"Dropdown has different value selected than 'Discharged'"
        self.do_send_keys(self.SEARCH_INPUT_ALL_PATIENTS, patient_id)
        self.driver.find_element(By.XPATH, "//input[@placeholder='Search by name, email or phone number']").send_keys(
                Keys.ENTER)
        time.sleep(3)
        print("Search field is set")
        date_of_admission = self.get_elements(self.DATE_OF_ADMISSION_TEXT)
        date_of_admission_list = []
        for admission in date_of_admission:
            date_of_admission_list.append(admission.text)
        assert '' not in date_of_admission_list, f"Date of admission has some blank dates"
        date_of_discharge = self.get_elements(self.DATE_OF_DISCHARGE_TEXT)
        date_of_discharge_list = []
        for discharge in date_of_discharge:
            date_of_discharge_list.append(discharge.text)
        assert '' not in date_of_discharge_list, f"Date of discharge has some blank dates"
        assert self.is_visible(self.PATIENT_HYPERLINK_ALL_PATIENT)==True, f"Patient link is not available"
        removed_patient_all_patients = self.get_elements(self.PATIENT_NAME_ALL_PATIENTS)
        removed_patient_name_list = []
        for name in removed_patient_all_patients:
            removed_patient_name_list.append(name.text)
        assert patient_id in removed_patient_name_list, f"Searched patient id is not available in ALL Patients"
        self.do_click(self.ADMITTED_DISCHARGED_DROPDOWN)
        self.do_click(self.ADMITTED_DROPDOWN_OPTION)
        print("Admitted option is selected")
        self.driver.execute_script("arguments[0].click();", search_button)
        print("Search button is clicked")
        time.sleep(3)
        from_calender_date = self.driver.find_element(By.XPATH, "//label[text()='From']//following::input[1]")
        to_calender_date = self.driver.find_element(By.XPATH, "//label[text()='To']//following::input[1]")
        time.sleep(3)
        min_date = datetime.strptime(from_calender_date.get_attribute("value"), '%d/%m/%Y')
        max_date = datetime.strptime(to_calender_date.get_attribute("value"), '%d/%m/%Y')
        number_of_days = max_date - min_date
        assert number_of_days.days == 15, "Date difference in calender is not 15 days"
        date_of_admission = self.get_elements(self.DATE_OF_ADMISSION_TEXT)
        date_of_admission_list = []
        for admission in date_of_admission:
            date_of_admission_list.append(admission.text)
        assert '' not in date_of_admission_list, f"Date of admission has some blank dates"
        date_of_discharge = self.get_elements(self.DATE_OF_DISCHARGE_TEXT)
        date_of_discharge_list = []
        for discharge in date_of_discharge:
            date_of_discharge_list.append(discharge.text)
        assert '' not in date_of_discharge_list, f"Date of discharge has some blank dates"
        assert self.is_visible(self.PATIENT_HYPERLINK_ALL_PATIENT) == True, f"Patient link is not available"
        removed_patient_all_patients = self.get_elements(self.PATIENT_NAME_ALL_PATIENTS)
        removed_patient_name_list = []
        for name in removed_patient_all_patients:
            removed_patient_name_list.append(name.text)
        assert patient_id in removed_patient_name_list, f"Searched patient id is not available in ALL Patients"
        self.do_click(self.PATIENT_HYPERLINK_ALL_PATIENT)
        print("Patient link is clicked")
        patient_id_text = self.get_element_text(self.PATIENT_ID_TEXT)
        assert patient_id in patient_id_text, f"Patient ID is not same in patient page"
        time.sleep(3)




    def add_patients_non_mandatory_fields(self, dozee_id):
        first_name = ['test', 'testing', 'automationtesting', 'automation', 'Testing']
        email = ""
        phone = ""
        patient_name = ""
        patient_id = ""
        for num in range(1):
            first = random.choice(first_name)
            patient = random.choice(first_name)
            id = first.lower() + str(random.randint(100, 999))
            p = f'{random.randint(100, 999)}555{random.randint(1000, 9999)}'
            e1 = first.lower() + str(random.randint(100, 999)) + '@gmail.com'
            email+=str(e1)
            phone+=str(p)
            patient_name+=str(patient)
            patient_id+=str(id)
        self.do_click(self.VITAL_TAB)
        print("Vitals tab is clicked")
        time.sleep(4)
        self.do_click(self.ADD_PATIENTS_BUTTON)
        print("ADD Patient button is clicked")
        self.do_send_keys(self.FIRST_NAME, patient_name)
        print("First name is set")
        # self.do_send_keys(self.LAST_NAME, last_name)
        self.do_send_keys(self.PATIENT_ID, patient_id)
        print("Patient ID is set")
        self.do_send_keys(self.EMAIL, email)
        self.do_send_keys(self.PHONE, phone)
        # self.do_click(self.NEXT_BUTTON_ADD_PATIENT)
        s = self.driver.find_elements(By.TAG_NAME, "button")
        for i in s:
            if i.text == "NEXT":
                i.click()
        print("Next button is clicked")
        doctor_name = 'DR. Dre'
        self.do_send_keys(self.DOCTOR_NAME_INPUT, doctor_name)
        # dozee_id = 'DP-3941'
        self.do_click(self.DOZEE_AVAILABLE_DEVICES)
        self.do_send_keys(self.DOZEE_SEARCH_BOX_ADD_PATIENT, dozee_id)
        if self.driver.find_element(By.XPATH, "//span[text() =' " + dozee_id + " ']"):
            self.driver.find_element(By.XPATH, "//span[text() =' " + dozee_id + " ']").click()
            print("Dozee device is set")
        else:
            print("Dozee device is not available")
        time.sleep(5)
        ward = self.driver.find_element(By.XPATH, "//mat-select[@name='WardNumber']//span[1]").text
        print("Ward number is: "+ward)
        bed = self.driver.find_element(By.XPATH, "//input[@name='BedNumber']").get_attribute('value')
        print("Bed number is: "+bed)
        for i in s:
            if i.text == "NEXT":
                i.click()
        medical_condition = 'Diabetes'
        self.do_send_keys(self.MEDICAL_CONDITION_INPUT, medical_condition)
        assert self.driver.find_element(By.XPATH,
                                         "//span[text() =' " + medical_condition + " ']"), f"Medical condition is not available"
        self.driver.find_element(By.XPATH, "//span[text() =' " + medical_condition + " ']").click()
        print("Medical condition is set")
        for i in s:
            if i.text == "NEXT":
                i.click()
        self.do_click(self.HIGH_HEART_RATE_TOGGLE)
        print("High heart rate alert is turned off")
        time.sleep(3)
        assert self.is_visible(self.HEART_RATE_TEXT) == True, f"Heart rate toggle is not available"
        assert self.is_visible(self.RESPIRATION_RATE_TEXT) == True, f"Respiration rate toggle is not available"
        assert self.is_visible(self.SPO2_RATE_TEXT) == True, f"SPO2 rate toggle is not available"
        assert self.is_visible(self.BP_SYSTOLIC_TEXT) == True, f"BP Systolic toggle is not available"
        assert self.is_visible(self.DEWS_TEXT) == True, f"DEWS toggle is not available"
        assert self.is_visible(self.TEMPERATURE_TEXT) == True, f"Temperature BP Systolic toggle is not available"
        assert self.is_visible(self.BED_OCCUPANCY_TEXT) == True, f"Bed Occupancy toggle is not available"
        assert self.is_visible(self.MOVEMENT_TEXT) == True, f"Movement toggle is not available"
        toggle_count = self.get_elements(self.TOGGLE)
        print("Number of toggle available is: " +str(len(toggle_count)))
        assert len(toggle_count)==8, f"Toggle count is not correct"
        # for i in s:
        #     if i.text == "Next":
        #         i.click()
        self.driver.find_element(By.XPATH, "//button[text()='Next']").click()
        print("Next button is clicked")
        escalate_email = 'test665647@gmail.com'
        self.do_send_keys(self.ESCALATE_INPUT_FIELD, escalate_email)
        print("Escalate is set")
        self.do_click(self.ADD_BUTTON)
        self.do_click(self.CLOSE_BUTTON_ADD_PATIENT)
        print("Close button is clicked")
        self.driver.refresh()
        time.sleep(3)
        self.do_send_keys(self.INPUT_FIELD_VITAL, dozee_id)
        print("Patient name is set")
        self.driver.find_element(By.XPATH, "//nb-select[@id= 'filterByName']//following::input[1]").send_keys(
            Keys.ENTER)
        time.sleep(3)
        assert self.driver.find_elements(By.XPATH,
                                         "//p[text() ='" + patient_id + "']"), f"New patient is not available"
        self.driver.find_element(By.XPATH, "//a[@class='appearance-ghost size-medium status-info shape-rectangle font-16 font-bold ng-star-inserted']").click()
        print("Patient name is clicked")
        time.sleep(5)
        device_name = self.get_element_text(self.DEVICE_NAME_PATIENT_PAGE)
        patient_id_text = self.get_element_text(self.PATIENT_NAME_PATIENT_PAGE)
        medical_condition_text = self.get_element_text(self.MEDICAL_CONDITION_TEXT)
        ward_number_text = self.get_element_text(self.WARD_NAME_TEXT)
        bed_number_text = self.get_element_text(self.BED_NUMBER_TEXT)
        doctor_name_text = self.get_element_text(self.DOCTOR_NAME_TEXT)

        assert dozee_id in device_name, f"Dozee ID is not same in patient page"
        assert patient_id in patient_id_text, f"Patient ID is not same in patient page"
        assert medical_condition in medical_condition_text, f"Medical condition is not same in patient page"
        # assert ward in ward_number_text, f"Ward number is not same in patient page"
        assert bed in bed_number_text, f"Bed number is not same in patient page"
        assert doctor_name in doctor_name_text, f"Doctor name is not same in patient page"
        self.do_click(self.ALERT_BUTTON_PATIENT_PAGE)
        print("Alert button is clicked")
        alerttext = self.get_elements(self.ALERT_TEXT_PATIENT_PAGE)
        alert_text_list = []
        for name in alerttext:
            alert_text_list.append(name.text)
        count_inactive = alert_text_list.count('INACTIVE')
        count_active = alert_text_list.count('ACTIVE')
        print("Number of 'Inactive' alert is : " + str(count_inactive))
        print("Number of 'Active' alert is : " + str(count_active))
        # assert count_inactive==2, f"'INACTIVE' alerts are more than 2"
        notifier_text = self.get_elements(self.NOTIFIER_TEXT)
        notifier_text_list = []
        for name in notifier_text:
            notifier_text_list.append(name.text)
        print(notifier_text_list)
        assert escalate_email in notifier_text_list, f"Notifier are not correct"
        time.sleep(4)


    def action_functionality(self, dozee_id):
        self.do_click(self.VITAL_TAB)
        print("Vitals tab is clicked")
        time.sleep(4)
        # dozee_id = 'DP-3941'
        self.do_send_keys(self.INPUT_FIELD_VITAL, dozee_id)
        self.driver.find_element(By.XPATH, "//nb-select[@id= 'filterByName']//following::input[1]").send_keys(
            Keys.ENTER)
        time.sleep(3)
        range_of_pages_and_patients = self.get_element(self.RANGE_OF_PAGES_OUT_OF_PATIENTS).text
        assert '1 – 1 of 1' in range_of_pages_and_patients, f"Range of pages out of patients is not correct"
        self.do_click(self.ACTION_BUTTON)
        print("Action button is clicked")
        self.do_click(self.REPORTS_BUTTON)
        print("Reporst button is clicked")
        reports_option_text = self.get_elements(self.PATIENT_REPORT_OPTIONS)
        reports_option_text_list = []
        report_actual_option_text = ['Last 3 hours', 'Last 8 hours', 'Last 24 hours', 'Custom', 'Full Report']
        for name in reports_option_text:
            reports_option_text_list.append(name.text)
        assert report_actual_option_text == reports_option_text_list, f"Patient reports options are more than 5"
        self.do_click(self.LAST_24_HOURS_RADIO_BUTTON)
        self.do_click(self.REQUEST_BUTTON)
        time.sleep(4)
        error_msg = self.driver.find_elements(By.TAG_NAME, "p")
        time.sleep(2)
        error_msg_list = []
        for i in error_msg:
            error_msg_list.append(i.text)
        if 'Report generation failed. Please check if there is data available for the selected duration and try again' not in error_msg_list:
            time.sleep(5)
            self.driver.switch_to.window(self.driver.window_handles[0])
            time.sleep(4)
            self.do_click(self.CANCEL_BUTTON)
            print("Cancel button is clicked")
        else:
            print("Error message is : " + self.get_element_text(self.ERROR_TEXT))
            self.do_click(self.CANCEL_BUTTON)
            print("Cancel button is clicked")
        self.do_click(self.ACTION_BUTTON)
        print("Action button is clicked")
        self.do_click(self.MANAGE_ALERTS_BUTTON)
        print("Manage alerts button is clicked")
        time.sleep(3)
        self.do_click(self.HEART_RATE_TEXT)
        # button1 = self.driver.find_element(By.XPATH,
        #                                    "//span[text()=' Low Heart Rate ']//following::div[4]")
        # self.driver.execute_script("arguments[0].click();", button1)
        print("Heart rate toggle is clicked")
        assert self.is_visible(self.HEART_RATE_TEXT) == True, f"Heart rate toggle is not available"
        assert self.is_visible(self.RESPIRATION_RATE_TEXT) == True, f"Respiration rate toggle is not available"
        assert self.is_visible(self.SPO2_RATE_TEXT) == True, f"SPO2 rate toggle is not available"
        assert self.is_visible(self.BP_SYSTOLIC_TEXT) == True, f"BP Systolic toggle is not available"
        assert self.is_visible(self.DEWS_TEXT) == True, f"DEWS toggle is not available"
        assert self.is_visible(self.TEMPERATURE_TEXT) == True, f"Temperature BP Systolic toggle is not available"
        assert self.is_visible(self.BED_OCCUPANCY_TEXT) == True, f"Bed Occupancy toggle is not available"
        assert self.is_visible(self.MOVEMENT_TEXT) == True, f"Movement toggle is not available"
        toggle_count = self.get_elements(self.TOGGLE)
        print("Number of toggle available is: " + str(len(toggle_count)))
        assert len(toggle_count) == 8, f"Toggle count is not correct"
        # for i in s:
        #     if i.text == "Next":
        #         i.click()
        self.driver.find_element(By.XPATH, "//button[text()='Next']").click()
        print("Next button is clicked")
        escalate_email = 'test665647@gmail.com'
        self.do_send_keys(self.ESCALATE_INPUT_FIELD, escalate_email)
        print("Escalate is set")
        self.do_click(self.ADD_BUTTON)
        print("Add button is clicked")
        button = self.driver.find_element(By.XPATH, "//button[@class='appearance-outline size-small status-primary shape-round transitions']")
        self.driver.execute_script("arguments[0].click();", button)
        print("Close button is clicked")
        time.sleep(4)
        self.do_click(self.ACTION_BUTTON)
        print("Action button is clicked")
        self.do_click(self.ADD_INFO_BUTTON)
        print("Add info button is clicked")
        self.do_click(self.ADD_NOTES_BUTTON)
        print("Add notes button is clicked")
        add_notes = 'automation testing'
        self.do_send_keys(self.ADD_NOTES_INPUT, add_notes)
        time.sleep(3)
        self.do_click(self.SAVE_BUTTON_ADD_NOTES)
        time.sleep(2)
        self.do_click(self.ACTION_BUTTON)
        print("Action button is clicked")
        self.do_click(self.ADD_INFO_BUTTON)
        print("Add info button is clicked")
        self.do_click(self.ADD_DATA_BUTTON)
        print("Add data button is clicked")
        time.sleep(2)
        tempe = '99'
        self.do_send_keys(self.TEMP_INPUT, tempe)
        print("Temperature is set")
        self.do_click(self.SAVE_BUTTON_ADD_NOTES)
        print("Data is saved")
        time.sleep(2)
        assert self.driver.find_elements(By.XPATH,
                                         "//a[@class='appearance-ghost size-medium status-info shape-rectangle font-16 font-bold ng-star-inserted']"), f"New patient is not available"
        self.driver.find_element(By.XPATH, "//a[@class='appearance-ghost size-medium status-info shape-rectangle font-16 font-bold ng-star-inserted']").click()
        print("Patient is clicked")
        self.do_click(self.NOTES_PATIENT_PAGE)
        print("Notes button is clicked")
        self.do_click(self.ADD_NOTES_BUTTON_PATIENT_PAGE)
        print("Add notes button is clicked")
        add_notes = 'automation testing'
        self.do_send_keys(self.ADD_NOTES_INPUT, add_notes)
        time.sleep(3)
        self.do_click(self.SAVE_BUTTON_ADD_NOTES)
        time.sleep(2)
        self.do_click(self.ADD_DATA_BUTTON_PATIENT_PAGE)
        print("Add data button is clicked")
        time.sleep(2)
        tempe1 = '87'
        self.do_click(self.MIN_DOWNGRADE_BUTTON)
        self.do_send_keys(self.TEMP_INPUT, tempe1)
        print("Temperature is set")
        self.do_click(self.SAVE_BUTTON_ADD_NOTES)
        print("Data is saved")
        time.sleep(2)
        notes_text = self.get_elements(self.NOTES_TEXT)
        data_text = self.get_elements(self.DATA_TEXT)
        notes_list = []
        data_list = []
        for item in notes_text:
            notes_list.append(item.text)
        assert add_notes in notes_list, f"Notes are not same in patient page"

        for temperature in data_text:
            data_list.append(temperature.text)
        assert tempe and tempe1 in data_list, f"Data are not same in patient page"

        self.is_visible(self.ALERT_BUTTON_PATIENT_PAGE)
        button1 = self.driver.find_element(By.XPATH,
                                                        "//h6[text()='Sleeps']//following::div[3]")
        self.driver.execute_script("arguments[0].click();", button1)
        print("Alert button is clicked")
        alerttext = self.get_elements(self.ALERT_TEXT_PATIENT_PAGE)
        alert_text_list = []
        for name in alerttext:
            alert_text_list.append(name.text)
        print(alert_text_list)
        # count_inactive = alert_text_list.count('INACTIVE')
        assert 'INACTIVE' in alert_text_list, f"'INACTIVE' alerts are not available"
        self.do_click(self.ADD_ESCALATION)
        print("Add escalation is clicked")
        # self.do_click(self.ALERT_ESCALATION_DROPDOWN)
        # print("Alert escalation dropdown is clicked")
        # self.do_click(self.PHONE_OPTION)
        # print("Mobile option is selected")
        escalate_phone = '+919898999898'
        self.do_send_keys(self.ESCALATE_INPUT_FIELD, escalate_phone)
        print("Mobile number is set")
        self.do_click(self.ADD_BUTTON_ESCALATION)
        print("Add button is clicked")
        self.do_click(self.CLOSE_BUTTON_ADD_PATIENT)
        print("Close button is clicked")
        notifier_text = self.get_elements(self.NOTIFIER_TEXT)
        notifier_text_list = []
        for name in notifier_text:
            notifier_text_list.append(name.text)
        print(notifier_text_list)
        assert escalate_phone and escalate_email in notifier_text_list, f"Notifier are not correct"
        self.driver.back()
        time.sleep(5)
        self.driver.find_element(By.XPATH, "//nb-select[@id= 'filterByName']//following::input[1]").send_keys(
            Keys.ENTER)
        time.sleep(3)
        snooze_msg = self.driver.find_elements(By.TAG_NAME, "p")
        time.sleep(2)
        snooze_msg_list = []
        for i in snooze_msg:
            snooze_msg_list.append(i.text)
        if 'Snoozed' in snooze_msg_list:
            time.sleep(5)
            self.do_click(self.ACTION_BUTTON)
            print("Action button is clicked")
            self.do_click(self.MODIFIY_SNOOZE)
            print("Modify snooze is clicked")
            self.do_click(self.MODIFIY_SNOOZE_DURATION)
            self.do_click(self.ZERO_MINUTES_OPTION)
            print("Zero minutes option is set")
            self.do_click(self.SNOOZE_BUTTON)
            print("Snooze button is clicked")
            time.sleep(4)
            snooze_msg2 = self.driver.find_elements(By.TAG_NAME, "p")
            time.sleep(2)
            snooze_msg_list2 = []
            for i in snooze_msg2:
                snooze_msg_list2.append(i.text)
            assert 'Snoozed' not in snooze_msg_list2, f"Unable to snooze the device"
            print("Device is unsnoozed")
        else:
            self.do_click(self.ACTION_BUTTON)
            print("Action button is clicked")
            self.do_click(self.SNOOZE_BUTTON_ACTION)
            print("Snooze button is clicked")
            snooze_option = self.driver.find_element(By.XPATH,
                                                     "//nb-card-header[text()='Snooze For']//following::span[2]").text
            print("Snooze time is: " + snooze_option)
            self.do_click(self.SNOOZE_BUTTON)
            print("Snooze button is clicked")
            assert self.get_element_text(self.SNOOZE_TEXT) == 'Snoozed', f"Device is not snoozed"
            print("Device is Snoozed")


    def patient_page(self, patient_name):
        self.do_click(self.VITAL_TAB)
        print("Vitals tab is clicked")
        time.sleep(4)
        # dozee_id = 'DP-3941'
        self.do_send_keys(self.INPUT_FIELD_VITAL, patient_name)
        self.driver.find_element(By.XPATH, "//nb-select[@id= 'filterByName']//following::input[1]").send_keys(
            Keys.ENTER)
        time.sleep(3)
        assert self.driver.find_elements(By.XPATH,
                                         "//a[text() =' " + patient_name + " ']"), f"New patient is not available"
        self.driver.find_element(By.XPATH, "//a[text() =' " + patient_name + " ']").click()
        print("Patient name is clicked")
        time.sleep(5)
        overview_text = self.get_elements(self.OVERVIEW_BOX_HEAD_TEXT)
        overview_text_list = []
        overview_actual_text = ['Recent', 'Last hour', 'Last 8 hours', 'Last 24 hours']
        for name in overview_text:
            overview_text_list.append(name.text)
        print(overview_text_list)
        assert overview_actual_text == overview_text_list, f"Overview boxes header is not correct"
        assert len(self.get_elements(self.BPM_TEXT))==4, f"BPM count is more than 4"
        assert len(self.get_elements(self.RPM_TEXT))==4, f"RPM count is more than 4"
        assert len(self.get_elements(self.SPO2_TEXT))==4, f"SpO2 count is more than 4"
        assert len(self.get_elements(self.TEMP_TEXT))==4, f"Temperature count is more than 4"
        assert len(self.get_elements(self.ALERTS_TEXT))==4, f"Temperature count is more than 4"
        daily_date_input = self.driver.find_element(By.XPATH, "//span[text()='Daily']//following::input[2]").get_attribute('value')
        if daily_date_input !="":
            self.do_click(self.DOWNLOAD_DATA_BUTTON)
            self.do_click(self.DOWNLOAD_AS_PDF)
            reports_option_text = self.get_elements(self.PATIENT_REPORT_OPTIONS)
            reports_option_text_list = []
            report_actual_option_text = ['Last 3 hours', 'Last 8 hours', 'Last 24 hours', 'Custom', 'Full Report']
            for name in reports_option_text:
                reports_option_text_list.append(name.text)
            print(reports_option_text_list)
            assert report_actual_option_text == reports_option_text_list, f"Patient reports options are more than 5"
            self.do_click(self.LAST_24_HOURS_RADIO_BUTTON)
            self.do_click(self.REQUEST_BUTTON)
            error_msg = self.driver.find_elements(By.TAG_NAME, "p")
            error_msg_list = []
            for i in error_msg:
                error_msg_list.append(i.text)
            if error_msg_list!='Report generation failed. Please check if there is data available for the selected duration and try again':
                time.sleep(5)
                self.driver.switch_to.window(self.driver.window_handles[0])
                time.sleep(4)
                self.do_click(self.CANCEL_BUTTON)
                print("Cancel button is clicked")
            else:
                print("Error message is : " + self.get_element_text(self.ERROR_TEXT))
                self.do_click(self.CANCEL_BUTTON)
            self.do_click(self.DOWNLOAD_DATA_BUTTON)
            print("Download data button is clicked")
            self.do_click(self.DOWNLOAD_AS_CSV)
            self.do_click(self.REQUEST_BUTTON)
            csv_report_from = self.driver.find_element(By.XPATH,
                                                        "//div[text()=' From ']//following::input[1]").get_attribute(
                'value')
            csv_report_to = self.driver.find_element(By.XPATH,
                                                        "//div[text()=' To ']//following::input[1]").get_attribute(
                'value')
            print("CSV report is available From : " + csv_report_from)
            print("CSV report is available To : " + csv_report_to)
            time.sleep(5)
            # self.do_click(self.DOWNLOAD_BUTTON_CSV_REPORT)
            self.do_click(self.CANCEL_BUTTON)
            time.sleep(4)
        else:
            print("Daily data input field is empty")

        self.do_click(self.TRENDS_BUTTON)
        print("Trend button is clicked")
        trend_data_from = self.driver.find_element(By.XPATH,
                                                        "//span[text()='Trends']//following::input[4]").get_attribute(
                'value')
        trend_data_to = self.driver.find_element(By.XPATH,
                                                   "//span[text()='Trends']//following::input[5]").get_attribute(
            'value')
        print("Trend data is available From : " + trend_data_from)
        print("Trend data is available To : " + trend_data_to)
        assert self.is_visible(self.SHOW_BUTTON)==True, f"Show button is not available"
        self.do_click(self.DOWNLOAD_DATA_TRENDS_BUTTON)
        self.do_click(self.DOWNLOAD_AS_PDF)
        print("Download as PDF is clicked")
        time.sleep(4)
        self.is_visible(self.SLEEP_PATIENT_BUTTON)
        button1 = self.driver.find_element(By.XPATH,
                                           "//h6[text()='Sleeps']/..")
        self.driver.execute_script("arguments[0].click();", button1)
        print("Sleep button is clicked")
        assert self.is_visible(self.WAKUP_TIME)==True, f"Wakup Time column is not available"
        assert self.is_visible(self.SLEEP_SCORE) == True, f"Sleep score column is not available"
        assert self.is_visible(self.HEART_RATE_SLEEP) == True, f"Heart rate column is not available"
        assert self.is_visible(self.RESPIRATION_RATE_SLEEP) == True, f"Respiration rate column is not available"
        assert self.is_visible(self.RECOVERY_SLEEP) == True, f"Recovery column is not available"
        assert self.is_visible(self.STRESS_SLEEP) == True, f"Stress column is not available"
        self.is_visible(self.REPORTS_PATIENT_PAGE)
        button_report = self.driver.find_element(By.XPATH,
                                           "//button[@class='appearance-outline size-medium status-info shape-semi-round transitions']")
        self.driver.execute_script("arguments[0].click();", button_report)
        print("Reports button is clicked")
        time.sleep(4)



    def vitals_color(self, dozee_id):
        self.do_click(self.VITAL_TAB)
        print("Vitals tab is clicked")
        time.sleep(4)
        # dozee_id = 'DOZ-4844'
        self.do_send_keys(self.INPUT_FIELD_VITAL, dozee_id)
        self.driver.find_element(By.XPATH, "//nb-select[@id= 'filterByName']//following::input[1]").send_keys(
            Keys.ENTER)
        time.sleep(3)
        respiration_rgb = self.driver.find_element(By.XPATH,
                                       "/html/body/ngx-app/ngx-pages/ngx-one-column-layout/nb-layout/div[1]/div/div/div/div/nb-layout-column/ngx-vitals/nb-card/nb-card-body/table/tbody/tr/td[6]/span").value_of_css_property(
            'color')
        respiration_text = self.driver.find_element(By.XPATH,
                                       "/html/body/ngx-app/ngx-pages/ngx-one-column-layout/nb-layout/div[1]/div/div/div/div/nb-layout-column/ngx-vitals/nb-card/nb-card-body/table/tbody/tr/td[6]/span").text
        print("Respiration rate is: "+respiration_text)
        if any(i.isdigit() for i in respiration_text):
            resp_text = respiration_text.split(" ")[0]
            respiration_rate = int(resp_text)
            resp_hex = Color.from_string(respiration_rgb).hex
            print("Respiration rate color hex code is : "+resp_hex)
            temp_dict = {'#f77f3c': 'Orange', '#cf0000': 'Red', '#ffb800': 'Yellow', '#000000': 'Black'}

            if resp_hex in temp_dict.keys():
                color_resp = temp_dict[resp_hex]
                if respiration_rate in range(21, 26) or respiration_rate in range(9, 12):
                    assert color_resp == 'Yellow', f"Respiration rate color is not Yellow"
                    print("Respiration rate color is Yellow")
                elif respiration_rate in range(6, 9) or respiration_rate in range(26, 31):
                    assert color_resp == 'Orange', f"Respiration rate color is not Orange"
                    print("Respiration rate color is Orange")
                elif respiration_rate in range(12, 21):
                    assert color_resp == 'Black', f"Respiration rate color is not Black"
                    print("Respiration rate color is Black")
                elif respiration_rate < 6 or respiration_rate > 30:
                    assert color_resp == 'Red', f"Respiration rate color is not Red"
                    print("Respiration rate color is Red")
                else:
                    raise AssertionError("Respiration rate is out of range with : "+ respiration_text)
            else:
                raise AssertionError("Respiration rate has wrong color assigned to it")
        else:
            print("Respiration has no values")

        heart_rate_rgb = self.driver.find_element(By.XPATH,
                                       "/html/body/ngx-app/ngx-pages/ngx-one-column-layout/nb-layout/div[1]/div/div/div/div/nb-layout-column/ngx-vitals/nb-card/nb-card-body/table/tbody/tr/td[5]/span").value_of_css_property(
            'color')
        heart_rate_text = self.driver.find_element(By.XPATH,
                                       "/html/body/ngx-app/ngx-pages/ngx-one-column-layout/nb-layout/div[1]/div/div/div/div/nb-layout-column/ngx-vitals/nb-card/nb-card-body/table/tbody/tr/td[5]/span").text
        print("Heart rate is: "+heart_rate_text)
        if any(i.isdigit() for i in heart_rate_text):
            heart_text = heart_rate_text.split(" ")[0]
            heart_rate = int(heart_text)
            heart_rate_hex = Color.from_string(heart_rate_rgb).hex
            print("Heart rate color hex code is : "+heart_rate_hex)

            if heart_rate_hex in temp_dict.keys():
                color_heart = temp_dict[heart_rate_hex]
                if heart_rate in range(51, 61) or heart_rate in range(91, 101):
                    assert color_heart == 'Yellow', f"Heart rate color is  not Yellow"
                    print("Heart rate color is Yellow")
                elif heart_rate in range(40, 51) or heart_rate in range(101, 121):
                    assert color_heart == 'Orange', f"Heart rate color is  not Orange"
                    print("Heart rate color is Orange")
                elif heart_rate in range(61, 91):
                    assert color_heart == 'Black', f"Heart rate color is  not Black"
                    print("Heart rate color is Black")
                elif heart_rate < 40 or heart_rate > 120:
                    assert color_heart == 'Red', f"Heart rate color is  not Red"
                    print("Heart rate color is Red")
                else:
                    raise AssertionError("Heart rate is out of range with : "+ heart_rate_text)
            else:
                raise AssertionError("Heart rate has wrong color assigned to it")
        else:
            print("Heart rate has no value")

        spo2_rgb = self.driver.find_element(By.XPATH,
                                                   "/html/body/ngx-app/ngx-pages/ngx-one-column-layout/nb-layout/div[1]/div/div/div/div/nb-layout-column/ngx-vitals/nb-card/nb-card-body/table/tbody/tr/td[7]/div/span").value_of_css_property(
            'color')
        spo2_text = self.driver.find_element(By.XPATH,
                                                    "/html/body/ngx-app/ngx-pages/ngx-one-column-layout/nb-layout/div[1]/div/div/div/div/nb-layout-column/ngx-vitals/nb-card/nb-card-body/table/tbody/tr/td[7]/div/span").text
        print("SpO2 rate is: "+spo2_text)
        if any(i.isdigit() for i in spo2_text):
            spo2_value = spo2_text.split(" ")[0]
            spo2 = int(spo2_value)
            spo2_hex = Color.from_string(spo2_rgb).hex
            print("SpO2 rate color hex code is : "+spo2_hex)

            if spo2_hex in temp_dict.keys():
                color_spo2 = temp_dict[spo2_hex]
                if spo2 in range(93, 95):
                    assert color_spo2 == 'Yellow', f"SpO2 rate color is not Yellow"
                    print("SpO2 rate color is Yellow")
                elif spo2 in range(88, 93):
                    assert color_spo2 == 'Orange', f"SpO2 rate color is not Orange"
                    print("SpO2 rate color is Orange")
                elif spo2 in range(95, 101):
                    assert color_spo2 == 'Black', f"SpO2 rate color is not Black"
                    print("SpO2 rate color is Black")
                elif spo2 < 88:
                    assert color_spo2 == 'Red', f"SpO2 rate color is not Red"
                    print("SpO2 rate color is Red")
                else:
                    raise AssertionError("SpO2 rate is out of range with : "+ spo2_text)
            else:
                raise AssertionError("SpO2 rate has wrong color assigned to it")
        else:
            print("SpO2 has no values")

        nibp_rgb = self.driver.find_element(By.XPATH,
                                            "/html/body/ngx-app/ngx-pages/ngx-one-column-layout/nb-layout/div[1]/div/div/div/div/nb-layout-column/ngx-vitals/nb-card/nb-card-body/table/tbody/tr/td[7]/div/span//following::span[1]").value_of_css_property(
            'color')
        nibp_text = self.driver.find_element(By.XPATH,
                                             "/html/body/ngx-app/ngx-pages/ngx-one-column-layout/nb-layout/div[1]/div/div/div/div/nb-layout-column/ngx-vitals/nb-card/nb-card-body/table/tbody/tr/td[7]/div/span//following::span[1]").text
        print("NiBP rate is: "+nibp_text)
        if any(i.isdigit() for i in nibp_text):
            nibp_value = nibp_text.split(" ")[0]
            nibp1 = nibp_value.split("/")[0]
            nibp = int(nibp1)
            nibp_hex = Color.from_string(nibp_rgb).hex
            print("NiBP rate color hex code is : "+nibp_hex)

            if nibp_hex in temp_dict.keys():
                color_nibp = temp_dict[nibp_hex]
                if nibp in range(141, 161) or nibp in range(90, 101):
                    assert color_nibp == 'Yellow', f"NiBP rate color is not Yellow"
                    print("NiBP rate color is Yellow")
                elif nibp in range(85, 90) or nibp in range(161, 181):
                    assert color_nibp == 'Orange', f"NiBP rate color is not Orange"
                    print("NiBP rate color is Orange")
                elif nibp in range(101, 141):
                    assert color_nibp == 'Black', f"NiBP rate color is not Black"
                    print("NiBP rate color is Black")
                elif nibp < 85 or nibp > 180:
                    assert color_nibp == 'Red', f"NiBP rate color is not Red"
                    print("NiBP rate color is Red")
                else:
                    raise AssertionError("NiBP rate is out of range with : "+ nibp_text)
            else:
                raise AssertionError("NiBP rate has wrong color assigned to it")
        else:
            print("NiBP has no value")

        temperature_rgb = self.driver.find_element(By.XPATH,
                                            "/html/body/ngx-app/ngx-pages/ngx-one-column-layout/nb-layout/div/div/div/div/div/nb-layout-column/ngx-vitals/nb-card/nb-card-body/table/tbody/tr/td[9]/span").value_of_css_property(
            'color')
        temperature_text = self.driver.find_element(By.XPATH,
                                             "/html/body/ngx-app/ngx-pages/ngx-one-column-layout/nb-layout/div/div/div/div/div/nb-layout-column/ngx-vitals/nb-card/nb-card-body/table/tbody/tr/td[9]/span").text
        print("Temperature is: " + temperature_text)

        temperature_value = temperature_text.split(" ")[0]
        if any(i.isdigit() for i in temperature_text):
            temperature = float(temperature_value)
            temperature_hex = Color.from_string(temperature_rgb).hex
            print("DEWS rate color hex code is : " + temperature_hex)

            if temperature_hex in temp_dict.keys():
                color_temperature = temp_dict[temperature_hex]
                if 98.6 <= temperature <= 100.5:
                    assert color_temperature == 'Yellow', f"Temperature rate color is not Yellow"
                    print("Temperature rate color is Yellow")
                elif 94 <= temperature <= 96 or 100.6 <= temperature <= 102:
                    assert color_temperature == 'Orange', f"Temperature rate color is not Orange"
                    print("Temperature rate color is Orange")
                elif 96 <= temperature <= 98.5:
                    assert color_temperature == 'Black', f"Temperature rate color is not Black"
                    print("Temperature rate color is Black")
                elif temperature < 94 or temperature > 102:
                    assert color_temperature == 'Red', f"Temperature rate color is not Red"
                    print("Temperature rate color is Red")
                else:
                    raise AssertionError("Temperature is out of range with : " + temperature_text)
            else:
                raise AssertionError("Temperature has wrong color assigned to it")
        else:
            print("Temperature has no value")

        dews_rgb = self.driver.find_element(By.XPATH,
                                            "/html/body/ngx-app/ngx-pages/ngx-one-column-layout/nb-layout/div[1]/div/div/div/div/nb-layout-column/ngx-vitals/nb-card/nb-card-body/table/tbody/tr/td[4]/span").value_of_css_property(
            'color')
        dews_text = self.driver.find_element(By.XPATH,
                                             "/html/body/ngx-app/ngx-pages/ngx-one-column-layout/nb-layout/div[1]/div/div/div/div/nb-layout-column/ngx-vitals/nb-card/nb-card-body/table/tbody/tr/td[4]/span").text
        print("DEWS rate is: "+dews_text)
        if any(i.isdigit() for i in dews_text):
            dews_value = dews_text.split(" ")[-1]
            dews = int(dews_value)
            dews_hex = Color.from_string(dews_rgb).hex
            print("DEWS rate color hex code is : "+dews_hex)

            if dews_hex in temp_dict.keys():
                color_dews = temp_dict[dews_hex]
                if dews == 2:
                    assert color_dews == 'Yellow', f"DEWS rate color is not Yellow"
                    print("DEWS rate color is Yellow")
                elif dews == 3:
                    assert color_dews == 'Orange', f"DEWS rate color is not Orange"
                    print("DEWS rate color is Orange")
                elif dews == 0 or dews == 1:
                    assert color_dews == 'Black', f"DEWS rate color is not Black"
                    print("DEWS rate color is Black")
                elif dews in range(4, 8):
                    assert color_dews == 'Red', f"DEWS rate color is not Red"
                    print("DEWS rate color is Red")
                else:
                    raise AssertionError("DEWS rate is out of range with : "+ dews_text)
            else:
                raise AssertionError("DEWS rate has wrong color assigned to it")
        else:
            print("DEWS rate has no values")


    def bp_calibrate(self):
        self.do_click(self.VITAL_TAB)
        print("Vitals tab is clicked")
        time.sleep(4)
        dozee_id = 'DP-6057'
        self.do_send_keys(self.INPUT_FIELD_VITAL, dozee_id)
        self.driver.find_element(By.XPATH, "//nb-select[@id= 'filterByName']//following::input[1]").send_keys(
            Keys.ENTER)
        time.sleep(3)
        self.do_click(self.BP_CALIBRATE)
        print("BP Calibrate link is clicked")
        self.do_send_keys(self.AVERAGE_SYSTOLIC_BP, "130")
        print("Average Systolic BP is set")
        self.do_send_keys(self.AVERAGE_DIASTOLIC_BP, "60")
        print("Average Diastolic BP is set")
        self.do_send_keys(self.AVERAGE_HEART_RATE, "85")
        print("Average heart rate is set")
        self.do_click(self.CALIBRATE_BUTTON)
        print("Calibrate button is clicked")
        time.sleep(4)

    def patient_expired(self, dozee_id):
        patient_id = 'testing12345'
        self.do_send_keys(self.INPUT_FIELD_VITAL, dozee_id)
        print("Patient name is set")
        self.driver.find_element(By.XPATH, "//nb-select[@id= 'filterByName']//following::input[1]").send_keys(
            Keys.ENTER)
        time.sleep(2)
        assert self.driver.find_elements(By.XPATH, "//p[text() ='" + patient_id + "']"), f"New patient is not available"
        self.do_click(self.ACTION_BUTTON)
        print("Action button is clicked")
        self.do_click(self.REMOVE_PATIENT_BUTTON)
        print("Remove patient button is clicked")
        self.do_click(self.PATIENT_EXPIRED_RADIO_BUTTON)
        print("Patient expired radio button is clicked")
        time.sleep(4)
        self.do_click(self.YES_BUTTON)
        print("Yes button is clicked")
        time.sleep(4)
        self.do_click(self.ALL_PATIENTS_TAB)
        print("All Patients tab is clicked")
        self.is_visible(self.DOWNLOAD_BUTTON_ALL_PATIENT)
        download_button = self.get_element(self.DOWNLOAD_BUTTON_ALL_PATIENT)
        self.driver.execute_script("arguments[0].click();", download_button)
        print("Download button is clicked")
        removed_patient_all_patients = self.get_elements(self.PATIENT_NAME_ALL_PATIENTS)
        removed_patient_name_list = []
        for name in removed_patient_all_patients:
            removed_patient_name_list.append(name.text)
        assert patient_id in removed_patient_name_list, f"Removed patient is not available in ALL Patients"
        print("Removed patient successfully")


    def shifted_to_icu(self, dozee_id):
        patient_id = 'testing12345'
        self.do_send_keys(self.INPUT_FIELD_VITAL, dozee_id)
        print("Patient name is set")
        self.driver.find_element(By.XPATH, "//nb-select[@id= 'filterByName']//following::input[1]").send_keys(
            Keys.ENTER)
        time.sleep(2)
        assert self.driver.find_elements(By.XPATH, "//p[text() ='" + patient_id + "']"), f"New patient is not available"
        self.do_click(self.ACTION_BUTTON)
        print("Action button is clicked")
        self.do_click(self.REMOVE_PATIENT_BUTTON)
        print("Remove patient button is clicked")
        self.do_click(self.SHIFTED_To_ICU_RADIO_BUTTON)
        print("Shifted to ICU radio button is clicked")
        time.sleep(4)
        self.do_click(self.YES_BUTTON)
        print("Yes button is clicked")
        time.sleep(4)
        self.do_click(self.ALL_PATIENTS_TAB)
        print("All Patients tab is clicked")
        self.is_visible(self.DOWNLOAD_BUTTON_ALL_PATIENT)
        download_button = self.get_element(self.DOWNLOAD_BUTTON_ALL_PATIENT)
        self.driver.execute_script("arguments[0].click();", download_button)
        print("Download button is clicked")
        removed_patient_all_patients = self.get_elements(self.PATIENT_NAME_ALL_PATIENTS)
        removed_patient_name_list = []
        for name in removed_patient_all_patients:
            removed_patient_name_list.append(name.text)
        assert patient_id in removed_patient_name_list, f"Removed patient is not available in ALL Patients"
        print("Removed patient successfully")

    def shifted_to_other_ward_or_bed(self, dozee_id):
        patient_id = 'testing12345'
        self.do_send_keys(self.INPUT_FIELD_VITAL, dozee_id)
        print("Patient name is set")
        self.driver.find_element(By.XPATH, "//nb-select[@id= 'filterByName']//following::input[1]").send_keys(
            Keys.ENTER)
        time.sleep(2)
        assert self.driver.find_elements(By.XPATH, "//p[text() ='" + patient_id + "']"), f"New patient is not available"
        self.do_click(self.ACTION_BUTTON)
        print("Action button is clicked")
        self.do_click(self.REMOVE_PATIENT_BUTTON)
        print("Remove patient button is clicked")
        self.do_click(self.SHIFTED_To_OTHER_WARD_OR_BED_RADIO_BUTTON)
        print("Shifted to other Ward/Bed radio button is clicked")
        time.sleep(4)
        self.do_click(self.YES_BUTTON)
        print("Yes button is clicked")
        time.sleep(4)
        self.do_click(self.ALL_PATIENTS_TAB)
        print("All Patients tab is clicked")
        self.is_visible(self.DOWNLOAD_BUTTON_ALL_PATIENT)
        download_button = self.get_element(self.DOWNLOAD_BUTTON_ALL_PATIENT)
        self.driver.execute_script("arguments[0].click();", download_button)
        print("Download button is clicked")
        removed_patient_all_patients = self.get_elements(self.PATIENT_NAME_ALL_PATIENTS)
        removed_patient_name_list = []
        for name in removed_patient_all_patients:
            removed_patient_name_list.append(name.text)
        assert patient_id in removed_patient_name_list, f"Removed patient is not available in ALL Patients"
        print("Removed patient successfully")


    def shifted_to_another_dozee(self, dozee_id, shifted_to_device):
        patient_id = 'testing12345'
        self.do_send_keys(self.INPUT_FIELD_VITAL, dozee_id)
        print("Patient name is set")
        self.driver.find_element(By.XPATH, "//nb-select[@id= 'filterByName']//following::input[1]").send_keys(
            Keys.ENTER)
        time.sleep(2)
        assert self.driver.find_elements(By.XPATH, "//p[text() ='" + patient_id + "']"), f"New patient is not available"
        self.do_click(self.ACTION_BUTTON)
        print("Action button is clicked")
        self.do_click(self.REMOVE_PATIENT_BUTTON)
        print("Remove patient button is clicked")
        self.do_click(self.SHIFTED_To_ANOTHER_DOZEE)
        print("Shifted to another dozee radio button is clicked")
        time.sleep(4)
        self.do_click(self.YES_BUTTON)
        print("Yes button is clicked")
        self.do_click(self.DEVICE_CONTROL_DROPDOWN)
        self.do_send_keys(self.SEARCH_DEVICE_INPUT, shifted_to_device)
        assert self.driver.find_element(By.XPATH, "//span[text() =' " + shifted_to_device + " ']"),f"Dozee device is not available"
        self.driver.find_element(By.XPATH, "//span[text() =' " + shifted_to_device + " ']").click()
        print("New Dozee device is set")
        time.sleep(4)
        ward = self.driver.find_element(By.XPATH, "//mat-select[@name='WardNumber']").get_attribute('textContent')
        print("Ward number is: " + ward)
        bed = self.driver.find_element(By.XPATH, "//input[@name='BedNumber']").get_attribute('value')
        print("Bed number is: " + bed)
        self.do_click(self.CONFIRM_BUTTON)
        print("Confirm button is clicked")
        self.do_click(self.REFRESH_BUTTON)
        time.sleep(3)
        self.do_send_keys(self.INPUT_FIELD_VITAL, shifted_to_device)
        print("Patient name is set")
        self.driver.find_element(By.XPATH, "//nb-select[@id= 'filterByName']//following::input[1]").send_keys(
            Keys.ENTER)
        time.sleep(2)
        assert self.driver.find_elements(By.XPATH, "//p[text() ='" + patient_id + "']"), f"New patient is not available"
        ward_name = self.driver.find_element(By.XPATH, "//td[@class='mat-cell cdk-column-BedNumber mat-column-BedNumber ng-star-inserted']").text
        assert ward in ward_name, f"Ward name is not same"

