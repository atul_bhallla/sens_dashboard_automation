from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from Config.config import TestData
from Pages.BasePage import BasePage
import time
from Pages.EmailOTP import Login


class LoginPage(BasePage):
    """By Locators"""
    EMAIL_BUTTON = (By.ID,"mat-tab-label-0-1")
    EMAIL_INPUT = (By.ID,"mat-input-1")
    REQUEST_OTP = (By.XPATH,"//*[@id=\"mat-tab-content-0-1\"]/div/div/form/div[3]/button")
    OTP_INPUT = (By.TAG_NAME,"input")
    VERIFY_BUTTON = (By.CLASS_NAME, "verify_button")

    """constructor of the page class"""
    def __init__(self, driver):
        super().__init__(driver)
        self.driver.get(TestData.QA_Testing_Base_URL)

    def get_login_page_title(self, title):
        return self.get_title(title)

    """Page Actions for Login Page"""

    def get_otp(self):
        x = Login()
        x.read_email_OTP_from_gmail()
        return x.otp

    def do_login(self, email):
        actions = ActionChains(self.driver)
        if self.is_visible(self.EMAIL_BUTTON):
            time.sleep(3)
            self.do_click(self.EMAIL_BUTTON)
            self.do_send_keys(self.EMAIL_INPUT, email)
            self.is_visible(self.REQUEST_OTP)
            self.driver.find_element(By.XPATH,"//*[@id=\"mat-tab-content-0-1\"]/div/div/form/div[3]/button").click()
            time.sleep(5)
            if self.is_visible(self.OTP_INPUT):
                otp = self.get_otp()
                self.do_send_keys(self.OTP_INPUT,otp)
                self.is_visible(self.VERIFY_BUTTON)
                button = self.get_element(self.VERIFY_BUTTON)
                ActionChains(self.driver).click(button).perform()