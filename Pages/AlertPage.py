import time
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from Pages.BasePage import BasePage

class AlertPage(BasePage):
    ALERT_TAB = (By.XPATH, "//a[@title='Alerts']")
    MORE_ALERTS_COLUMN = (By.XPATH, "//button[text()='More Alerts']")
    DEVICE_COLUMN = (By.XPATH, "//button[text()='Device']")
    PATIENT_COLUMN = (By.XPATH, "//button[text()='Patient']")
    ALERT_DATE_COLUMN = (By.XPATH, "//button[text()='Alert Date']")
    ALERT_REASON_COLUMN = (By.XPATH, "//button[text()='Alert Reason']")
    OBSERVED_VALUE_COLUMN = (By.XPATH, "//button[text()='Observed Value']")
    THRESHOLD_VALUE_COLUMN = (By.XPATH, "//button[text()='Threshold Value']")
    AVERAGE_OF_LAST_COLUMN = (By.XPATH, "//button[text()='Average of Last']")
    DOWNLOAD_BUTTON = (By.XPATH, "//button[text()=' Download ']")
    ALERTS_TABLE = (By.XPATH, "//tr[@class='mat-row ng-star-inserted']")
    SEARCH_FIELD = (By.XPATH, "//input[@placeholder='Search by name, email or device no']")
    RANGE_OF_PAGES_OUT_OF_PATIENTS = (By.XPATH, "//div[@class='mat-paginator-range-label']")
    DEVICE_NAME_TEXT = (By.XPATH, "//td[@class='mat-cell cdk-column-DeviceName mat-column-DeviceName ng-star-inserted']")
    MORE_ALERTS_LINK = (By.XPATH, "//button[text()='Average of Last']//following::a[1]")
    REFRESH_BUTTON = (By.XPATH, "//button[@class='right-margin appearance-filled size-tiny status-info shape-round icon-start transitions']")


    def __init__(self, driver):
        super().__init__(driver)

    def alert_page_functionality(self, dozee_id):
        self.do_click(self.ALERT_TAB)
        print("Alert tab is clicked")
        time.sleep(3)
        assert self.is_visible(self.MORE_ALERTS_COLUMN) == True, f"More alerts column is not available"
        assert self.is_visible(self.DEVICE_COLUMN) == True, f"Device column is not available"
        assert self.is_visible(self.PATIENT_COLUMN) == True, f"Patient column is not available"
        assert self.is_visible(self.ALERT_DATE_COLUMN) == True, f"Alert date column is not available"
        assert self.is_visible(self.ALERT_REASON_COLUMN) == True, f"Alert reason column is not available"
        assert self.is_visible(self.OBSERVED_VALUE_COLUMN) == True, f"Observed value column is not available"
        assert self.is_visible(self.THRESHOLD_VALUE_COLUMN) == True, f"Threshold value column is not available"
        assert self.is_visible(self.AVERAGE_OF_LAST_COLUMN) == True, f"Average of last column is not available"
        self.do_click(self.DOWNLOAD_BUTTON)
        print("Download button is clicked")
        self.is_visible(self.REFRESH_BUTTON)
        print("Refresh button is available")
        time.sleep(5)
        alerts_data = self.get_elements(self.ALERTS_TABLE)
        alerts_data_list = []
        for data in alerts_data:
            alerts_data_list.append(data.text)
        if 'Bed not occupied' in alerts_data_list:
            assert '--' in alerts_data_list, f"Empty values are not available when 'Bed not occupied'"
        self.do_send_keys(self.SEARCH_FIELD, dozee_id)
        self.driver.find_element(By.XPATH, "//input[@placeholder='Search by name, email or device no']").send_keys(
            Keys.ENTER)
        time.sleep(3)
        print("Search field is set")
        device_name = self.get_elements(self.DEVICE_NAME_TEXT)
        device_name_list = []
        for name in device_name:
            device_name_list.append(name.text)
        assert dozee_id in device_name_list, "Device name is not same in results"
        print("Device name is available in result")
        self.do_click(self.MORE_ALERTS_LINK)
        print("More link is clicked")
        device_name2 = self.get_elements(self.DEVICE_NAME_TEXT)
        device_name_list2 = []
        for name in device_name2:
            device_name_list2.append(name.text)
        assert dozee_id in device_name_list2, "Device name is not same in results"
        self.do_click(self.DOWNLOAD_BUTTON)
        print("Download button is clicked")
        # range_of_pages_and_patients = self.get_element_text(self.RANGE_OF_PAGES_OUT_OF_PATIENTS)
        # assert '1 – 1 of 1' in range_of_pages_and_patients, f"Range of pages out of patients is not correct"
        time.sleep(7)
        data_column = self.driver.find_element(By.XPATH, "//tr[@class='mat-header-row ng-star-inserted']").text
        # print(data_column.replace('\n', ' '))
        data_column_list = ['Device', 'Patient', 'Alert Date', 'Alert Reason', 'Observed Value', 'Threshold Value', 'Average of Last']
        for i in data_column_list:
            if i in data_column.replace('\n', ' '):
                assert True, "All columns are available"
            else:
                assert False, f"Some columns are not available"
        alerts_data2 = self.get_elements(self.ALERTS_TABLE)
        alerts_data_list2 = []
        for data in alerts_data2:
            alerts_data_list2.append(data.text)
        if 'Bed not occupied' in alerts_data_list2:
            assert '--' in alerts_data_list2, f"Empty values are not available when 'Bed not occupied'"