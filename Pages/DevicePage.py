import time

from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.color import Color

from Pages.BasePage import BasePage
from selenium.webdriver.common.keys import Keys

class DevicePage(BasePage):

    DEVICE_TAB = (By.XPATH, "//a[@title='Devices']")
    ERROR_MESSAGE_EXPIRED_DEVICE = (By.XPATH, "//h5[text()=' One or more device(s) license is expiring or expired.']")
    LIVE_INPUT_FIELD = (By.XPATH, "//input[@placeholder='Search by name, email or device no']")
    HEADER_PATIENT_COUNT = (By.XPATH, "//div[@class='div']")
    TAIL_PATIENT_COUNT = (By.XPATH,"//div[@class='mat-paginator-range-label']")
    DEVICE_NAME_TEXT = (By.XPATH, "//button[text()='Device Name']")
    PATIENT_TEXT = (By.XPATH, "//button[text()='Patient']")
    LAST_SYNC_TEXT = (By.XPATH, "//button[text()='Last Sync']")
    ADMITTED_TEXT = (By.XPATH, "//button[text()='Admitted On']")
    BED_NUMBER_TEXT = (By.XPATH, "//button[text()='Bed Number']")
    WARD_NUMBER_TEXT = (By.XPATH, "//button[text()='Ward Number']")
    PAIRING_STATUS = (By.XPATH, "//th[text()='Pairing Status']")
    BED_NUMBER_EDIT_BUTTON = (By.XPATH, "//th[text()='Pairing Status']//following::nb-icon[1]")
    WARD_NUMBER_EDIT_BUTTON = (By.XPATH, "//th[text()='Pairing Status']//following::nb-icon[2]")
    BED_NUMBER_INPUT_FIELD = (By.XPATH, "//input[@name='contentName']")
    SAVE_BUTTON = (By.XPATH, "//button[text()='SAVE']")
    WARD_NUMBER_DROPDOWN = (By.XPATH, "//div[text()='Select Ward']//following::mat-form-field[1]")
    DOWNLOAD_BUTTON = (By.XPATH, "//button[@class='right-margin appearance-filled size-small status-primary shape-round transitions']")
    ADD_DEVICES_TO_LIVE_VIEW_BUTTON = (By.XPATH, "//button[text()=' Add Devices to Live View ']")
    CHECKBOX_LIVE_PAGE = (By.XPATH, "//mat-checkbox[@class='mat-checkbox mat-accent']")
    UPDATED_SELECTED_DEVICES = (By.XPATH, "//button[text()=' Update Selected Devices ']")
    SUCCESS_POPUP_MESSAGE = (By.XPATH, "//div[text()=' Updated list of live devices successfully. ']")
    DEVICE_NAME_TEXT2 = (
    By.XPATH, "//td[@class='mat-cell cdk-column-DeviceName mat-column-DeviceName ng-star-inserted']")
    RANGE_OF_PAGES_OUT_OF_PATIENTS = (By.XPATH, "//div[@class='mat-paginator-range-label']")



    def __init__(self, driver):
        super().__init__(driver)

    def device_page_functionality(self, dozee_id):
        self.do_click(self.DEVICE_TAB)
        print("Device tab is clicked")
        time.sleep(2)
        self.do_click(self.DOWNLOAD_BUTTON)
        print("Download button is clicked")
        time.sleep(4)
        if self.driver.find_elements(By.XPATH, "//nb-icon[@class='status-danger']"):
            assert self.is_visible(self.ERROR_MESSAGE_EXPIRED_DEVICE)==True, f"Error message for expired device is not available"
        else:
            print("No expired devices available")
        time.sleep(4)
        pairing_status = self.driver.find_elements(By.XPATH, "//a[@nbtooltipplacement='top']")
        pairing_status_list = []
        for device in pairing_status:
            pairing_status_list.append(device.get_attribute("nbtooltipstatus"))
        print("Number of paired devices on Device page: "+ str(pairing_status_list.count('success')))
        print("Number of unpaired devices on Device page: " + str(pairing_status_list.count('basic')))
        head_patient_count = self.get_element_text(self.HEADER_PATIENT_COUNT)
        headpatient = head_patient_count.split(" ")[1]
        tail_patient_count = self.get_element_text(self.TAIL_PATIENT_COUNT)
        tailpatient = tail_patient_count.split(" ")[-1]
        assert headpatient == tailpatient, f"Number of patients in device page is not same"
        assert self.is_visible(self.DEVICE_NAME_TEXT)==True, f"Device name column is not available"
        assert self.is_visible(self.PATIENT_TEXT) == True, f"Patient name column is not available"
        assert self.is_visible(self.LAST_SYNC_TEXT) == True, f"Last Sync column is not available"
        assert self.is_visible(self.ADMITTED_TEXT) == True, f"Admitted On column is not available"
        assert self.is_visible(self.BED_NUMBER_TEXT) == True, f"Bed number column is not available"
        assert self.is_visible(self.WARD_NUMBER_TEXT) == True, f"Ward number column is not available"
        assert self.is_visible(self.PAIRING_STATUS) == True, f"Pairing status column is not available"
        self.do_send_keys(self.LIVE_INPUT_FIELD, dozee_id)
        print("Search input field is set")
        self.driver.find_element(By.XPATH,
                                 "//input[@placeholder='Search by name, email or device no']").send_keys(
            Keys.ENTER)
        time.sleep(3)
        device_status_rgb = self.driver.find_element(By.CSS_SELECTOR, "body > ngx-app > ngx-pages > ngx-one-column-layout > nb-layout > div.scrollable-container > div > div > div > div > nb-layout-column > ngx-devices > ngx-device-table > nb-card > nb-card-body > table > tbody > tr > td.mat-cell.cdk-column-DeviceName.mat-column-DeviceName.ng-star-inserted > span > svg > circle").value_of_css_property('fill')
        device_hex = Color.from_string(device_status_rgb).hex
        print("Device status color hex code is : " + device_hex)
        color_dict = {'#a8a8a8': 'Grey', '#00a500': 'Green'}
        if device_hex in color_dict.keys():
            color = color_dict[device_hex]
            if color == 'Grey':
                print("Device is offline")
            elif color == 'Green':
                print("Device is online")
            else:
                print("Device has different color or may be device is expired")
        self.do_click(self.BED_NUMBER_EDIT_BUTTON)
        print("Bed number edit button is clicked")
        self.driver.find_element(By.XPATH, "//input[@name='contentName']").clear()
        self.do_send_keys(self.BED_NUMBER_INPUT_FIELD, "47")
        self.do_click(self.SAVE_BUTTON)
        print("Save button is clicked")
        actions = ActionChains(self.driver)
        button = self.get_element(self.WARD_NUMBER_EDIT_BUTTON)
        ActionChains(self.driver).click(button).perform()
        print("Ward number edit button is clicked")
        time.sleep(4)
        self.do_click(self.WARD_NUMBER_DROPDOWN)
        # time.sleep(2)
        ward_number_option = 'Automation Test'
        button1 = self.driver.find_element(By.XPATH,
                                          "//span[text() ='" + ward_number_option + "']")
        ActionChains(self.driver).click(button1).perform()
        # self.driver.find_element(By.XPATH, "//span[text() ='" + ward_number_option + "']").click()
        print("Ward number is set")
        self.do_click(self.SAVE_BUTTON)
        print("Save button is clicked")
        time.sleep(2)
        self.driver.find_element(By.XPATH, "//input[@placeholder='Search by name, email or device no']").clear()
        self.driver.refresh()
        time.sleep(3)
        self.do_click(self.ADD_DEVICES_TO_LIVE_VIEW_BUTTON)
        print("Add Devices to live button is clicked")
        tail_patient_count2 = self.get_element_text(self.TAIL_PATIENT_COUNT)
        tailpatient2 = tail_patient_count2.split(" ")[-1]
        assert headpatient == tailpatient2, f"Number of patients in device page is not same"
        self.do_send_keys(self.LIVE_INPUT_FIELD, dozee_id)
        print("Device ID is set")
        self.do_click(self.LIVE_INPUT_FIELD)
        time.sleep(2)
        self.driver.find_element(By.XPATH, "//input[@placeholder='Search by name, email or device no']").send_keys(
            Keys.ENTER)
        range_of_pages_and_patients = self.get_element(self.RANGE_OF_PAGES_OUT_OF_PATIENTS).text
        assert '1 – 1 of 1' in range_of_pages_and_patients, f"Range of pages out of patients is not correct"
        dozee = self.get_element_text(self.DEVICE_NAME_TEXT2)
        assert dozee == dozee_id, f"Dozee ID is not same as searched"
        self.do_click(self.CHECKBOX_LIVE_PAGE)
        print("Device is selected")
        self.do_click(self.UPDATED_SELECTED_DEVICES)
        print("Updated selected devices button is clicked")
        assert self.is_visible(self.SUCCESS_POPUP_MESSAGE) == True, f"Device is not added to live"
        time.sleep(4)
        dozee2 = self.get_elements(self.DEVICE_NAME_TEXT2)
        dozee_id_list2 = []
        for name in dozee2:
            dozee_id_list2.append(name.text)
        assert dozee in dozee_id_list2, f"Dozee ID is not same as searched"
        print("Device is successfully added to live")