import time
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from Pages.BasePage import BasePage

class KnowledgeCenterPage(BasePage):
    KNOWLEDGE_CENTER_TAB = (By.XPATH, "//a[@title='Knowledge Center']")
    LINKS = (By.CLASS_NAME, "text-basic")
    HEADING = (By.CLASS_NAME, "blog-post-title-font")

    def __init__(self, driver):
        super().__init__(driver)

    def knowledge_center(self):
        global i
        self.do_click(self.KNOWLEDGE_CENTER_TAB)
        print("Knowledge center tab is clicked")
        check = 0
        url = ['https://www.dozee.health/post/blood-pressure',
               'https://www.dozee.health/post/resting-heart-rate-baseline-recommended-value',
               'https://www.dozee.health/post/resting-respiratory-rate-baseline-recommended-value',
               'https://www.dozee.health/post/sleep-stages-snoring-restlessness',
               'https://www.dozee.health/post/sleep-score-and-breakup',
               'https://www.dozee.health/post/stress',
               'https://www.dozee.health/post/recovery',
               'https://www.dozee.health/post/_sdnn',
               'https://www.dozee.health/post/rmssd',
               'https://www.dozee.health/post/heart-rate-variability-lf-hf',
               'https://www.dozee.health/post/dozee-early-warning-score-dews',
               'https://www.dozee.health/post/dozee-weekly-report',
               'https://www.dozee.health/post/sleep-apnea',
               'https://www.dozee.health/post/__mpi']
        links = self.get_elements(self.LINKS)
        for i in range(len(links)):
            link_text = links[i].text
            print("Clicked on this link : " +link_text)
            links[i].click()
            self.driver.switch_to.window(self.driver.window_handles[i + 1])
            head = self.get_element_text(self.HEADING)
            print("Heading of this URL : "+head)
            # print("test",self.driver.current_url in url)
            if self.driver.current_url not in url:
                check = i
            self.driver.switch_to.window(self.driver.window_handles[0])

        if check == 0:
            assert True, "All links in Knowledge Center points to right documents"
        else:
            assert False, f"{links[i]} points to wrong documents"
