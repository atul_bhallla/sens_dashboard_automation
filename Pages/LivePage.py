import time
from selenium.webdriver.common.by import By
from Pages.BasePage import BasePage
from selenium.webdriver.common.keys import Keys

class LivePage(BasePage):

    LIVE_TAB = (By.XPATH, "//a[@title='Live']")
    ADD_DEVICES_TO_LIVE_VIEW_BUTTON = (By.XPATH, "//button[text()=' Add Devices to Live View ']")
    LIVE_INPUT_FIELD = (By.XPATH, "//input[@placeholder='Search by name, email or device no']")
    CHECKBOX_LIVE_PAGE = (By.XPATH, "//mat-checkbox[@class='mat-checkbox mat-accent']")
    DEVICE_NAME_TEXT = (By.XPATH, "//td[@class='mat-cell cdk-column-DeviceName mat-column-DeviceName ng-star-inserted']")
    UPDATED_SELECTED_DEVICES = (By.XPATH, "//button[text()=' Update Selected Devices ']")
    SUCCESS_POPUP_MESSAGE = (By.XPATH, "//div[text()=' Updated list of live devices successfully. ']")
    LIVE_INPUT_FIELD2 = (By.XPATH, "//input[@placeholder='Search by name, email, phone number or device no']")
    ACTION_BUTTON = (By.XPATH, "//button[@aria-label='Action items']")
    MANAGE_ALERTS_BUTTON = (By.XPATH, "//span[text()='Manage Alerts']/../.")
    ADD_INFO_BUTTON = (By.XPATH, "//span[text()='Add Info']/../.")
    ADD_NOTES_BUTTON = (By.XPATH, "//button[text()='Add Notes']")
    SNOOZE_BUTTON_ACTION = (By.XPATH, "//span[text()='Snooze']/..")
    REMOVE_PATIENT_BUTTON = (By.XPATH, "//span[text()='Remove Patient']/../.")
    REPORTS_BUTTON = (By.XPATH, "//span[text()='Reports ']/../.")
    ADD_DATA_BUTTON = (By.XPATH, "//button[text()='Add Data']")
    DOWNLOAD_BUTTON = (By.XPATH,
                       "//button[@class='right-margin font-sm appearance-filled size-small status-primary shape-round transitions']")
    DEVICE_NAME_PATIENT_PAGE = (By.XPATH, "//p[@class='ng-star-inserted']")
    PATIENT_NAME_PATIENT_PAGE = (By.XPATH, "//p[@class='ng-star-inserted']//following::p[2]")
    MEDICAL_CONDITION_TEXT = (By.XPATH, "//div[@class='mat-chip-list-wrapper']")
    WARD_NAME_TEXT = (By.XPATH, "//div[@class='mat-chip-list-wrapper']//following::p[2]")
    BED_NUMBER_TEXT = (By.XPATH, "//div[@class='mat-chip-list-wrapper']//following::p[3]")
    DOCTOR_NAME_TEXT = (By.XPATH, "//div[@class='mat-chip-list-wrapper']//following::p[4]")
    RANGE_OF_PAGES_OUT_OF_PATIENTS = (By.XPATH, "/html/body/ngx-app/ngx-pages/ngx-one-column-layout/nb-layout/div[1]/div/div/div/div/nb-layout-column/ngx-time-view/nb-card/nb-card-body/mat-paginator/div/div/div[2]/div")

    def __init__(self, driver):
        super().__init__(driver)

    def add_devices_to_live_page(self, dozee_id):
        self.do_click(self.LIVE_TAB)
        print("Live tab is clicked")
        time.sleep(3)
        self.do_click(self.DOWNLOAD_BUTTON)
        print("Download button is clicked")
        dozee = self.get_elements(self.DEVICE_NAME_TEXT)
        dozee_id_list = []
        for name in dozee:
            dozee_id_list.append(name.text)
        if dozee_id not in dozee_id_list:
            self.do_click(self.ADD_DEVICES_TO_LIVE_VIEW_BUTTON)
            print("Add devices to live view button is clicked")
            self.do_send_keys(self.LIVE_INPUT_FIELD, dozee_id)
            print("Device ID is set")
            self.do_click(self.LIVE_INPUT_FIELD)
            time.sleep(2)
            self.driver.find_element(By.XPATH, "//input[@placeholder='Search by name, email or device no']").send_keys(
                Keys.ENTER)
            dozee = self.get_element_text(self.DEVICE_NAME_TEXT)
            assert dozee == dozee_id, f"Dozee ID is not same as searched"
            self.do_click(self.CHECKBOX_LIVE_PAGE)
            print("Device is selected")
            self.do_click(self.UPDATED_SELECTED_DEVICES)
            print("Updated selected devices button is clicked")
            assert self.is_visible(self.SUCCESS_POPUP_MESSAGE)==True, f"Device is not added to live"
            time.sleep(4)
            dozee2 = self.get_elements(self.DEVICE_NAME_TEXT)
            dozee_id_list2 = []
            for name in dozee2:
                dozee_id_list2.append(name.text)
            assert dozee in dozee_id_list2, f"Dozee ID is not same as searched"
            print("Device is successfully added to live")
            self.do_send_keys(self.LIVE_INPUT_FIELD2, dozee_id)
            self.driver.find_element(By.XPATH, "//input[@placeholder='Search by name, email, phone number or device no']").send_keys(
                Keys.ENTER)
            time.sleep(2)
            range_of_pages_and_patients = self.get_element(self.RANGE_OF_PAGES_OUT_OF_PATIENTS).text
            assert '1 – 1 of 1' in range_of_pages_and_patients, f"Range of pages out of patients is not correct"
            self.do_click(self.ACTION_BUTTON)
            print("Action button is clicked")
            assert self.is_visible(self.MANAGE_ALERTS_BUTTON)==True, f"Manage alerts button is not available"
            assert self.is_visible(self.SNOOZE_BUTTON_ACTION) == True, f"Snooze button is not available"
            assert self.is_visible(self.REPORTS_BUTTON) == True, f"Reports button is not available"
            assert self.is_visible(self.REMOVE_PATIENT_BUTTON) == True, f"Remove patient button is not available"
            assert self.is_visible(self.MANAGE_ALERTS_BUTTON) == True, f"Manage alerts button is not available"
            self.do_click(self.ADD_INFO_BUTTON)
            print("Add info button is clicked")
            assert self.is_visible(self.ADD_NOTES_BUTTON) == True, f"Add notes button is not available"
            assert self.is_visible(self.ADD_DATA_BUTTON) == True, f"Add data button is not available"
            time.sleep(2)
            button = self.driver.find_element(By.XPATH,
                                     "/html/body/ngx-app/ngx-pages/ngx-one-column-layout/nb-layout/div[1]/div/div/div/div/nb-layout-column/ngx-time-view/nb-card/nb-card-body/table/tbody/tr/td[2]/div/a")
            self.driver.execute_script("arguments[0].click();", button)
            print("Patient is clicked")
            device_name = self.get_element_text(self.DEVICE_NAME_PATIENT_PAGE)
            assert dozee_id in device_name, f"Dozee ID is not same in patient page"
            patient_id_text = self.get_element_text(self.PATIENT_NAME_PATIENT_PAGE)
            print(patient_id_text)
            # medical_condition_text = self.get_element_text(self.MEDICAL_CONDITION_TEXT)
            ward_number_text = self.get_element_text(self.WARD_NAME_TEXT)
            print(ward_number_text)
            bed_number_text = self.get_element_text(self.BED_NUMBER_TEXT)
            print(bed_number_text)
            doctor_name_text = self.get_element_text(self.DOCTOR_NAME_TEXT)
            print(doctor_name_text)

        else:
            print("Device is already added to live page")

