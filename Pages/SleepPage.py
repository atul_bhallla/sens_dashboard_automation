import time
from selenium.webdriver.common.by import By
from Pages.BasePage import BasePage
from selenium.webdriver.common.keys import Keys

class SleepPage(BasePage):

    LIVE_MODE_TOGGLE_BUTTON = (By.XPATH, "//div[@class='toggle']//mat-slide-toggle")
    SLEEP_HEADER_TEXT = (By.XPATH, "//nb-card-header[text()=' Sleeps ']")
    DEVICE_NAME_COLUMN_TEXT = (By.XPATH, "//button[text()=' Device Name ']")
    PATIENT_TEXT = (By.XPATH, "//button[text()=' Patient ']")
    SLEEPS_TEXT = (By.XPATH, "//button[text()=' Sleeps ']")
    LAST_SYNC = (By.XPATH, "//button[text()=' Last Sync ']")
    SCORE_TEXT = (By.XPATH, "//button[text()=' Score ']")
    HEART_TEXT = (By.XPATH, "//button[text()=' Heart ']")
    RESPIRATION_TEXT = (By.XPATH, "//button[text()=' Respiration ']")
    SLEEP_TEXT = (By.XPATH, "//button[text()=' Sleep ']")
    RECOVERY_TEXT = (By.XPATH, "//button[text()=' Recovery ']")
    STRESS_TEXT = (By.XPATH, "//button[text()=' Stress ']")
    SDNN_TEXT = (By.XPATH, "//button[text()=' SDNN ']")
    DOWNLOAD_BUTTON = (By.XPATH, "//button[@class='right-margin appearance-filled size-small status-primary shape-round transitions']")
    DEVICE_NAME = (By.XPATH, "//td[@class='mat-cell cdk-column-DeviceName mat-column-DeviceName ng-star-inserted']")
    PATIENT_NAME_TEXT = (By.XPATH, "//td[@class='mat-cell cdk-column-UserName mat-column-UserName ng-star-inserted']//a")
    LAST_SYNC_TIME = (By.XPATH, "//td[@class='mat-cell cdk-column-LastSyncedAt mat-column-LastSyncedAt ng-star-inserted']")
    SEARCH_INPUT_FIELD = (By.XPATH, "//input[@placeholder='Search by name, email or device no']")
    RANGE_OF_PAGES_OUT_OF_PATIENTS = (By.XPATH, "//div[@class='mat-paginator-range-label']")
    ITEMS_PER_PAGE = (By.XPATH, "//div[@class='mat-form-field-infix']//mat-select")
    ITEMS_PER_PAGE_OPTION = (By.TAG_NAME, "mat-option")
    TEN_ITEMS_PER_PAGE = (By.XPATH, "//div[@id='cdk-overlay-0']//mat-option[1]")
    SLEEP_COUNT = (By.XPATH, "//h6[text()='Sleeps']//following::div[1]")
    ALERTS_COUNT = (By.XPATH, "//h6[text()='Alerts']//following::div[1]")
    DEVICE_NAME_PATIENT_PAGE = (By.XPATH, "//p[@class='ng-star-inserted']")
    FIRST_WAKEUP_TIME = (By.XPATH, "//button[text()=' Stress ']//following::td[1]")
    WAKEUP_TIME_COLUMN = (By.XPATH, "//button[text()=' Wakup Time ']")
    SLEEP_SCORE_COLUMN = (By.XPATH, "//button[text()=' Sleep Score ']")
    HEART_RATE_COLUMN = (By.XPATH, "//button[text()=' Heart Rate ']")
    RESPIRATION_RATE_COLUMN = (By.XPATH, "//button[text()=' Respiration Rate ']")
    RECOVERY_COLUMN = (By.XPATH, "//button[text()=' Recovery ']")
    STRESS_COLUMN = (By.XPATH, "//button[text()=' Stress ']")
    SLEEP_TABLE = (By.XPATH, "//table[@class='mat-table mat-table']")
    BACK_BUTTON = (By.XPATH, "//a[@class='btn-light']")
    OPEN_CALENDER_BUTTON = (By.XPATH, "//button[@aria-label='Open calendar']")
    SCORE_HYPERLINK = (By.XPATH, "//a[text()=' Score ']")
    ALL_WEBSITES_TITLE = (By.CLASS_NAME, "blog-post-title-font")
    COMPARISON_TEXT = (By.XPATH, "//h5[text()=' Comparison ']")
    ALERTS_NOTIFICATION = (By.XPATH, "//div[@class='col-1 col-md-2 col-sm-3 alert-status']")
    ALERTS_TABLE = (By.XPATH, "//table//tr//td")
    DEVICE_COLUMN = (By.XPATH, "//button[text()='Device']")
    ALERT_DATE_COLUMN = (By.XPATH, "//button[text()='Alert Date']")
    ALERT_REASON_COLUMN = (By.XPATH, "//button[text()='Alert Reason']")
    OBSERVED_VALUE = (By.XPATH, "//button[text()='Observed Value']")
    THRESHOLD_VALUE = (By.XPATH, "//button[text()='Threshold Value']")
    AVERAGE_OF_LAST_COLUMN = (By.XPATH, "//button[text()='Average of Last']")
    DOWNLOAD_DATA_BUTTON = (By.XPATH, "//button[@class='right-margin appearance-filled size-small status-primary shape-round icon-start transitions']")


    def __init__(self, driver):
        super().__init__(driver)


    def verify_sleep_page(self, dozee_id):
        global i
        self.do_click(self.LIVE_MODE_TOGGLE_BUTTON)
        print("Live mode button is clicked")
        assert self.is_visible(self.SLEEP_HEADER_TEXT)==True, f"Sleep page is not available"
        print("Sleep page tab is selected")
        assert self.is_visible(self.DEVICE_NAME_COLUMN_TEXT)==True, f"Device name column is not available"
        assert self.is_visible(self.PATIENT_TEXT)==True, f"Patient column is not available"
        assert self.is_visible(self.SLEEPS_TEXT)==True, f"Sleeps column is not available"
        assert self.is_visible(self.LAST_SYNC)==True, f"Last sync column is not available"
        assert self.is_visible(self.SCORE_TEXT)==True, f"Score column is not available"
        assert self.is_visible(self.HEART_TEXT)==True, f"Heart column is not available"
        assert self.is_visible(self.RESPIRATION_TEXT)==True, f"Respiration column is not available"
        assert self.is_visible(self.SLEEP_TEXT)==True, f"Sleep column is not available"
        assert self.is_visible(self.RECOVERY_TEXT)==True, f"Recovery column is not available"
        assert self.is_visible(self.STRESS_TEXT)==True, f"Stress column is not available"
        assert self.is_visible(self.SDNN_TEXT)==True, f"SDNN column is not available"
        self.do_click(self.DOWNLOAD_BUTTON)
        print("Download button is clicked")
        device_name = self.get_elements(self.DEVICE_NAME)
        device_name_list = []
        for name in device_name:
            device_name_list.append(name.text)
        assert '' not in device_name_list, f"Device name column has blank values"
        patient_name = self.get_elements(self.PATIENT_NAME_TEXT)
        patient_name_list = []
        for patient in patient_name:
            patient_name_list.append(patient.text)
        assert '' not in patient_name_list, f"Patient name column has blank values"
        last_sync = self.get_elements(self.LAST_SYNC_TIME)
        last_sync_list = []
        for timing in last_sync:
            last_sync_list.append(timing.text)
        assert '' not in last_sync_list, f"Last sync column has blank values"
        self.do_click(self.ITEMS_PER_PAGE)
        print("Items per page dropdown is clicked")
        item_per_page_actual_option = ['10', '25', '50', '100']
        item_per_page_option = self.get_elements(self.ITEMS_PER_PAGE_OPTION)
        item_per_page_option_list = []
        for option in item_per_page_option:
            item_per_page_option_list.append(option.text)
        assert item_per_page_actual_option == item_per_page_option_list, f"Item per page has missing option"
        self.do_click(self.TEN_ITEMS_PER_PAGE)
        print("10 Items per page option is selected")
        range_of_pages_and_patients = self.get_element_text(self.RANGE_OF_PAGES_OUT_OF_PATIENTS)
        assert '1 – 10' in range_of_pages_and_patients, f"Range of pages out of patients is not correct"
        self.do_send_keys(self.SEARCH_INPUT_FIELD, dozee_id)
        self.driver.find_element(By.XPATH, "//input[@placeholder='Search by name, email or device no']").send_keys(
            Keys.ENTER)
        time.sleep(3)
        print("Search field is set")
        range_of_pages_and_patients = self.get_element_text(self.RANGE_OF_PAGES_OUT_OF_PATIENTS)
        assert '1 – 1 of 1' in range_of_pages_and_patients, f"Range of pages out of patients is not correct"
        patient_name_link = self.get_element(self.PATIENT_NAME_TEXT)
        self.driver.execute_script("arguments[0].click();", patient_name_link)
        print("Patient name is clicked")
        time.sleep(4)
        device_name = self.get_element_text(self.DEVICE_NAME_PATIENT_PAGE)
        assert dozee_id in device_name, f"Dozee ID is not same in patient page"
        sleeps_count = self.get_element_text(self.SLEEP_COUNT)
        if '--' not in sleeps_count:
            print("Sleeps count for the patient is : "+sleeps_count)
            self.do_click(self.SLEEP_COUNT)
            print("Sleeps buttons is clicked")
            first_wakeup_time = self.get_element_text(self.FIRST_WAKEUP_TIME)
            first_wakeup_time = first_wakeup_time.replace(',', '')
            first_wakeup_time = first_wakeup_time.split(" ")[0:3]
            time_str = ""
            for sleep in range(len(first_wakeup_time)):
                if sleep ==1:
                    time_str = time_str + first_wakeup_time[sleep] + "," + " "
                else:
                    time_str = time_str + first_wakeup_time[sleep] + " "
            time_str = time_str.split(" ")[0:3]
            time_str = " ".join(time_str)
            print(time_str)
            assert self.is_visible(self.WAKEUP_TIME_COLUMN)==True, "Wakeup time column is not available"
            assert self.is_visible(self.SLEEP_SCORE_COLUMN) == True, "Sleep score column is not available"
            assert self.is_visible(self.HEART_RATE_COLUMN) == True, "Heart rate column is not available"
            assert self.is_visible(self.RESPIRATION_RATE_COLUMN) == True, "Respiration rate column is not available"
            assert self.is_visible(self.RECOVERY_COLUMN) == True, "Recovery column is not available"
            assert self.is_visible(self.STRESS_COLUMN) == True, "Stress column is not available"
            time.sleep(8)
            table = self.get_element_text(self.SLEEP_TABLE)
            assert '--' not in table, f"Sleep data has missing values"
            self.do_click(self.BACK_BUTTON)
            print("Back button is clicked")
            self.do_click(self.OPEN_CALENDER_BUTTON)
            print("Calender is open")
            time.sleep(2)
            self.driver.find_element(By.XPATH, "//td[@aria-label='" + time_str + "']").click()
            # self.driver.find_element(By.XPATH, "//td[@aria-label='June 14, 2022']").click()
            print("Date is selected")
            time.sleep(4)
            self.do_click(self.DOWNLOAD_DATA_BUTTON)
            print("Download data button is clicked")
            assert self.is_visible(self.SCORE_HYPERLINK)==True, f"Data is not available for selected date"
            assert self.is_visible(self.COMPARISON_TEXT)==True, f"Comparison data is not available"
            check = 0
            url = ['https://www.dozee.health/post/sleep-score-and-breakup',
                   'https://www.dozee.health/post/summary',
                   'https://www.dozee.health/post/resting-heart-rate-baseline-recommended-value',
                   'https://www.dozee.health/post/resting-respiratory-rate-baseline-recommended-value',
                   'https://www.dozee.health/post/recovery',
                   'https://www.dozee.health/post/stress',
                   'https://www.dozee.health/post/sleep-stages-snoring-restlessness/',
                   'https://www.dozee.health/post/dozee-early-warning-score-dews',
                   'https://www.dozee.health/post/_sdnn',
                   'https://www.dozee.health/post/rmssd',
                   'https://www.dozee.health/post/heart-rate-variability-lf-hf']

            links = self.driver.find_elements(By.CLASS_NAME, "text-basic")
            for i in range(len(links)):
                text = links[i].text
                links[i].click()
                self.driver.switch_to.window(self.driver.window_handles[i + 1])
                time.sleep(5)
                text1 = self.get_element_text(self.ALL_WEBSITES_TITLE)
                print(text1)
                if self.driver.current_url not in url:
                    check = i
                self.driver.switch_to.window(self.driver.window_handles[0])
            if check == 0:
                assert True, "All links in Knowledge Center points to right documents"
            else:
                assert False, f"{links[i]} points to wrong documents"
        else:
            print("Sleeps are not available for this patient")

        alerts_count = self.get_element_text(self.ALERTS_COUNT)
        if '--' not in alerts_count:
            print("Simple alerts for the patient is : "+alerts_count)
            alerts_count_button = self.get_element(self.ALERTS_COUNT)
            self.driver.execute_script("arguments[0].click();", alerts_count_button)
            print("Simple alerts is clicked")
            time.sleep(7)
            alerts = self.get_elements(self.ALERTS_NOTIFICATION)
            alerts_list = []
            for alert in alerts:
                alerts_list.append(alert.text)
            count_inactive = alerts_list.count('INACTIVE')
            count_active = alerts_list.count('ACTIVE')
            print("Number of 'Inactive' alert is : " + str(count_inactive))
            print("Number of 'Active' alert is : " + str(count_active))
            assert count_active + count_inactive ==8 , f"Total 'INACTIVE' or 'ACTIVE' alerts are not 8"
            time.sleep(6)
            assert self.is_visible(self.DEVICE_COLUMN) == True, "Device column is not available"
            assert self.is_visible(self.ALERT_DATE_COLUMN) == True, "Alert Date column is not available"
            assert self.is_visible(self.ALERT_REASON_COLUMN) == True, "Alert reason column is not available"
            assert self.is_visible(self.OBSERVED_VALUE) == True, "Observed value column is not available"
            assert self.is_visible(self.THRESHOLD_VALUE) == True, "Threshold column is not available"
            assert self.is_visible(self.AVERAGE_OF_LAST_COLUMN) == True, "Average of last column is not available"
            time.sleep(4)
            alerts_data = self.get_elements(self.ALERTS_TABLE)
            alerts_data_list = []
            for data in alerts_data:
                alerts_data_list.append(data.text)
            if 'Bed not occupied' in alerts_data_list:
                assert '--' in alerts_data_list, f"Empty values are not available when 'Bed not occupied'"