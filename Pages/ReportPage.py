import time
from selenium.webdriver.common.by import By
from Pages.BasePage import BasePage
from datetime import datetime


class ReportPage(BasePage):
    REPORT_TAB = (By.XPATH, "//a[@title='Reports']")
    REPORTS_PAGE_HEADER = (By.XPATH, "//div[text()='Reports will be available only for 30 days']")
    REPORT_NAME_COLUMN = (By.XPATH, "//button[text()='Report Name']")
    EXPIRY_COLUMN = (By.XPATH, "//button[text()='Expiry']")
    DOWNLOAD_COLUMN = (By.XPATH, "//th[text()='Download']")
    REFRESH_BUTTON = (By.XPATH, "//button[text()=' Refresh ']")
    WARDCOUNT = (By.CLASS_NAME, "link-cursor")
    REPORTS_HYPERLINK = (By.XPATH, "//a[@class='font-13 font-bold pointer']")
    DOWNLOAD_BUTTONS = (By.XPATH, "//th[text()='Download']//following::nb-icon[@icon='download-outline']")

    def __init__(self, driver):
        super().__init__(driver)

    def report_page_functionality(self):
        global ward_name, name_of_report
        self.is_visible(self.WARDCOUNT)
        d1 = self.get_elements(self.WARDCOUNT)
        no_of_wards = len(d1)
        print("Number of wards on ward page : " + str(no_of_wards))
        self.do_click(self.REPORT_TAB)
        print("Reports tab is clicked")
        assert self.is_visible(self.REPORTS_PAGE_HEADER) == True, f"Reports page header is not available"
        assert self.is_visible(self.REPORT_NAME_COLUMN) == True, f"Reports column is not available"
        assert self.is_visible(self.EXPIRY_COLUMN) == True, f"Expiry column is not available"
        assert self.is_visible(self.DOWNLOAD_COLUMN) == True, f"Download column is not available"
        assert self.is_visible(self.REFRESH_BUTTON) == True, f"Refresh button is not available"
        calender_date = self.driver.find_element(By.XPATH, "//label[text()='From']//following::input[1]")
        time.sleep(3)
        min_date = datetime.strptime(calender_date.get_attribute("min"), '%Y-%m-%d')
        max_date = datetime.strptime(calender_date.get_attribute("max"), '%Y-%m-%d')
        number_of_days = max_date - min_date
        assert number_of_days.days == 30, "Date difference in calender is more than 30 days"
        assert self.get_elements(self.REPORTS_HYPERLINK) != '', f"Zero Reports available in All wards"
        no_of_reports = self.get_elements(self.REPORTS_HYPERLINK)
        print("Number of reports available in All wards : " + str(len(no_of_reports)))
        report_name_list = []
        for name in no_of_reports:
            report_name_list.append(name.text)
        print(*report_name_list,sep='\n')
        assert self.get_elements(self.DOWNLOAD_BUTTONS) != '', f"Download buttons are not available for reports"
        self.driver.back()
        for i in range(0, no_of_wards):
            self.is_visible(self.WARDCOUNT)
            wards_count = self.get_elements(self.WARDCOUNT)
            if i == 0:
                continue
            print("Clicked this ward : " + wards_count[i].text)
            ward_name = wards_count[i].text.split(" ")[0]
            wards_count[i].click()
            self.do_click(self.REPORT_TAB)
            print("Reports tab is clicked")
            time.sleep(5)
            try :
                if self.is_visible(self.REPORTS_HYPERLINK):
                    reports_name = self.get_elements(self.REPORTS_HYPERLINK)
                    reports_name_list = []
                    for names in reports_name:
                        reports_name_list.append(names.text)
                    reportname1 = reports_name_list[0]
                    name_of_report = reportname1.split("_")[0]
                    assert ward_name in name_of_report, f"Report name and Ward name are not same"
            except:
                print("Reports are not available for this ward : "+ ward_name)
            time.sleep(3)
            self.driver.back()
            self.driver.back()