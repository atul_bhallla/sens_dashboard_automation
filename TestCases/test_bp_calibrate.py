from Pages.VitalPage import VitalPage
from TestCases.test_LoginPage import Test_Login
from TestCases.test_base import BaseTest
import pytest


class Test_BP_Calibrate(BaseTest):

    def test_login(self):
        self.loginPage = Test_Login(self.driver)
        self.loginPage.test_login_page_title()
        self.loginPage.test_login()
        self.loginPage.test_header()

    # @pytest.mark.skip()
    def test_bp_calibrate(self):
        self.vitalPage = VitalPage(self.driver)
        self.vitalPage.bp_calibrate()