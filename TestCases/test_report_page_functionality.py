from Pages.ReportPage import ReportPage
from TestCases.test_LoginPage import Test_Login
from TestCases.test_base import BaseTest
import pytest


class Test_Report_Page(BaseTest):

    def test_login(self):
        self.loginPage = Test_Login(self.driver)
        self.loginPage.test_login_page_title()
        self.loginPage.test_login()
        self.loginPage.test_header()

    # @pytest.mark.skip()
    def test_report_page_functionality(self):
        self.devicePage = ReportPage(self.driver)
        self.devicePage.report_page_functionality()