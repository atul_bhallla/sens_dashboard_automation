from Pages.AlertPage import AlertPage
from TestCases.test_LoginPage import Test_Login
from TestCases.test_base import BaseTest
import pytest


class Test_Alert_page_functionality(BaseTest):

    def test_login(self):
        self.loginPage = Test_Login(self.driver)
        self.loginPage.test_login_page_title()
        self.loginPage.test_login()
        self.loginPage.test_header()

    # @pytest.mark.skip()
    def test_alert_page_functionality(self):
        self.alertPage = AlertPage(self.driver)
        dozee_id = 'DP-6376' #DOZ-4299
        self.alertPage.alert_page_functionality(dozee_id)

