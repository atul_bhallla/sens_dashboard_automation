from Pages.VitalPage import VitalPage
from TestCases.test_LoginPage import Test_Login
from TestCases.test_base import BaseTest
import pytest


class Test_Patient_Expired(BaseTest):

    def test_login(self):
        self.loginPage = Test_Login(self.driver)
        self.loginPage.test_login_page_title()
        self.loginPage.test_login()
        self.loginPage.test_header()

    # @pytest.mark.skip()
    def test_patient_expired_option(self):
        self.vitalPage = VitalPage(self.driver)
        # dozee_id = 'DOZ-6987'
        dozee_id = 'DP-3941'
        self.vitalPage.add_patients_mandatory_fields(dozee_id)
        self.vitalPage.patient_expired(dozee_id)