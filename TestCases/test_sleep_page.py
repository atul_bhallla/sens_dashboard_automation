from Pages.SleepPage import SleepPage
from TestCases.test_LoginPage import Test_Login
from TestCases.test_base import BaseTest
import pytest


class Test_Sleep_page_functionality(BaseTest):

    def test_login(self):
        self.loginPage = Test_Login(self.driver)
        self.loginPage.test_login_page_title()
        self.loginPage.test_login()
        self.loginPage.test_header()

    # @pytest.mark.skip()
    def test_sleep_page_functionality(self):
        self.sleepPage = SleepPage(self.driver)
        dozee_id = 'DOZ-4813' #DOZ-4299
        self.sleepPage.verify_sleep_page(dozee_id)

