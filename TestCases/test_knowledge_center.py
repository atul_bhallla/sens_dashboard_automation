from Pages.KnowledgeCenterPage import KnowledgeCenterPage
from TestCases.test_LoginPage import Test_Login
from TestCases.test_base import BaseTest
import pytest


class Test_Knowledge_Center(BaseTest):

    def test_login(self):
        self.loginPage = Test_Login(self.driver)
        self.loginPage.test_login_page_title()
        self.loginPage.test_login()
        self.loginPage.test_header()

    # @pytest.mark.skip()
    def test_knowledge_center(self):
        self.knowledgecenterPage = KnowledgeCenterPage(self.driver)
        self.knowledgecenterPage.knowledge_center()

