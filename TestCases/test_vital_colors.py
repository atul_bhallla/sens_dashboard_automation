from Pages.VitalPage import VitalPage
from TestCases.test_LoginPage import Test_Login
from TestCases.test_base import BaseTest
import pytest


class Test_Vital_Add_Patients(BaseTest):

    def test_login(self):
        self.loginPage = Test_Login(self.driver)
        self.loginPage.test_login_page_title()
        self.loginPage.test_login()
        self.loginPage.test_header()

    # @pytest.mark.skip()
    def test_vitals_colors(self):
        self.vitalPage = VitalPage(self.driver)
        dozee_id = 'DP-6376'
        # dozee_id = 'DOZ-4813'
        self.vitalPage.vitals_color(dozee_id)