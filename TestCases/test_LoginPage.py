import pytest
from Config.config import TestData
from Pages.BasePage import BasePage
from Pages.LoginPage import LoginPage
from Pages.WardPage import WardPage

class Test_Login(BasePage):

    def __init__(self, driver):
        super().__init__(driver)

    def test_login_page_title(self):
        self.loginPage = LoginPage(self.driver)
        title = self.loginPage.get_title(TestData.LOGIN_PAGE_TITLE)
        assert title==TestData.LOGIN_PAGE_TITLE

    def test_login(self):
        self.loginPage = LoginPage(self.driver)
        self.loginPage.do_login(TestData.email_id)

    def test_header(self):
        self.wardPage = WardPage(self.driver)
        header,account = self.wardPage.get_header()
        self.wardPage.get_org_name()
        assert account==TestData.ACCOUNT_Name