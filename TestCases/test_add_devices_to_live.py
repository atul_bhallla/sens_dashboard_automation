from Pages.LivePage import LivePage
from TestCases.test_LoginPage import Test_Login
from TestCases.test_base import BaseTest
import pytest


class Test_Add_Devices_To_Live(BaseTest):

    def test_login(self):
        self.loginPage = Test_Login(self.driver)
        self.loginPage.test_login_page_title()
        self.loginPage.test_login()
        self.loginPage.test_header()

    # @pytest.mark.skip()
    def test_add_patients_mandatory_fields(self):
        self.livePage = LivePage(self.driver)
        dozee_id = 'DP-6376'
        self.livePage.add_devices_to_live_page(dozee_id)